package br.com.mercadodemusica.enums;

public enum ProcessamentoCorreiosEnum {
	POSTADO("1");
	
	private final String valor;

	ProcessamentoCorreiosEnum(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
}
