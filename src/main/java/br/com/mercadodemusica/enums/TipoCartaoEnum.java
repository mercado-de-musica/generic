package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum TipoCartaoEnum implements Serializable{
	CREDITO("Crédito"),
	DEBITO("Débito");
	
	private String descricao;

	private TipoCartaoEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
}
