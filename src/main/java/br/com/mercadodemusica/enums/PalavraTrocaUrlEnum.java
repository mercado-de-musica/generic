package br.com.mercadodemusica.enums;

import br.com.mercadodemusica.dto.PalavraChaveDTO;

public enum PalavraTrocaUrlEnum {
	PALAVRA_PESQUISADA("palavraPesquisada", PalavraChaveDTO.class), NUMERO_PAGINA("numeroPagina", Integer.class);

	private String palavraString;

	private Object tipoEnum;

	private PalavraTrocaUrlEnum(String palavraString, Object tipoEnum) {
		this.palavraString = palavraString;
		this.tipoEnum = tipoEnum;
	}

	public String getPalavraString() {
		return palavraString;
	}

	public Object getTipoEnum() {
		return tipoEnum;
	}
}
