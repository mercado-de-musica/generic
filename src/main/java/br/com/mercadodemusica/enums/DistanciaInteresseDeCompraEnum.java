package br.com.mercadodemusica.enums;

public enum DistanciaInteresseDeCompraEnum {
	_0("Qualquer distância", 0),
	_50("Até 50 km", 50),
	_150("Até 150 km", 150),
	_300("Até 300 km", 300),
	_500("Até 500 km", 500);
	

	private String descricao;
	private Integer valor;

	private DistanciaInteresseDeCompraEnum(String descricao, Integer valor) {
		this.descricao = descricao;
		this.valor = valor;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Integer getValor() {
		return this.valor;
	}
}
