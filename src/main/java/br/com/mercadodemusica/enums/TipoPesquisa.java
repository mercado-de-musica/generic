package br.com.mercadodemusica.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TipoPesquisa {
	@JsonProperty("PRECO")
	PRECO("produto mais barato"),
	@JsonProperty("PROXIMIDADE")
	PROXIMIDADE("produto mais próximo");

	private String descricao;
	
	TipoPesquisa(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
