package br.com.mercadodemusica.enums;

import java.io.Serializable;


public enum SexoEnum implements Serializable{
	M("Masculino"),
	F("Feminino");
	
	private String descricao;

	private SexoEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Enum<SexoEnum>[] getValues() {
		return SexoEnum.values();
	}
}
