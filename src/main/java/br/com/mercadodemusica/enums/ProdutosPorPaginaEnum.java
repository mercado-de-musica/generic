package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum ProdutosPorPaginaEnum implements Serializable {
	
	_10("10"),
	_20("20"),
	_50("50"),
	_100("100");

	private final String descricao;

	ProdutosPorPaginaEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return descricao;
	}
}
