package br.com.mercadodemusica.enums;

public enum IntermediadorDePagamentoEnum {
	TRAY_CHECKOUT("Tray Checkout", 15);
	
	
	private String descricao;
	private Integer diasRecebimento;
	
	private IntermediadorDePagamentoEnum(String descricao, Integer diasRecebimento) {
		this.descricao = descricao;
		this.diasRecebimento = diasRecebimento;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Integer getDiasRecebimento() {
		return this.diasRecebimento;
	}
}
