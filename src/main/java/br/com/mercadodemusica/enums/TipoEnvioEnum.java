package br.com.mercadodemusica.enums;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoEnvioEnum implements Serializable{
	EM_MAOS("EM_MAOS", "A combinar entre as partes"),
	CORREIOS("CORREIOS", "Correios com frete pago pelo comprador");
	 
	private String sigla;
	private String descricao;

	private TipoEnvioEnum(String sigla, String descricao) {
		this.sigla = sigla;
		this.descricao = descricao;
	}  
	
	public String getSigla() {
		return this.sigla;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
}
