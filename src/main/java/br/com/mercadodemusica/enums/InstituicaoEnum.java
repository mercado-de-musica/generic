package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum InstituicaoEnum implements Serializable{
	VISA("Visa", "3"),
	MASTERCARD("Mastercard", "4"),
	AMEX("American Express", "5"),
	ELO("Elo", "16"),
	HIPERCARD("Hipercard", "20"),
	HIPER("Hiper", "25");
	
	private String descricao;
	
	private String numero;

	private InstituicaoEnum(String descricao, String numero) {
		this.descricao = descricao;
		this.numero = numero;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public String getNumero() {
		return this.numero;
	}
	
	public Enum<InstituicaoEnum>[] getValues() {
		return InstituicaoEnum.values();
	}
}
