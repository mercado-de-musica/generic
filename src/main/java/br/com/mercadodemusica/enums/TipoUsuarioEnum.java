package br.com.mercadodemusica.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TipoUsuarioEnum {
	@JsonProperty("ROLE_USER")
	ROLE_USER("usuario"), 
	@JsonProperty("ROLE_ADMIN")
	ROLE_ADMIN("administrador"),
	@JsonProperty("ROLE_CRAWLER")
	ROLE_CRAWLER("crawler");
	
	private String descricao;

	private TipoUsuarioEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Enum<TipoUsuarioEnum>[] getValues() {
		return TipoUsuarioEnum.values();
	}
}
