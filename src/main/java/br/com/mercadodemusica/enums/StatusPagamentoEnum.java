package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum StatusPagamentoEnum implements Serializable{
	Sucesso,
	Falha;
}
