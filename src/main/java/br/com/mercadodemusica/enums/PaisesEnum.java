package br.com.mercadodemusica.enums;

public enum PaisesEnum {
	BR("Brasil", "55");
	
	private String descricao;
	private String codigoPais;

	private PaisesEnum(String descricao, String codigoPais) {
		this.descricao = descricao;
		this.codigoPais = codigoPais;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public String getCodigoPais() {
		return this.codigoPais;
	}
}
