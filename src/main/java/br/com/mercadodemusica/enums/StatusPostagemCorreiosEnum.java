package br.com.mercadodemusica.enums;

public enum StatusPostagemCorreiosEnum {
	EmBranco ("0"),
	Postado ("1"),
	Cancelado ("2"),
	Estorno ("3"),
	Entregue ("4"),
	Desistido ("5");
	
	private String numeroCorreio;
	
	private StatusPostagemCorreiosEnum(String numeroCorreio) {
		this.numeroCorreio = numeroCorreio;
	}
	
	public String getNumeroCorreio() {
		return this.numeroCorreio;
	}
}
