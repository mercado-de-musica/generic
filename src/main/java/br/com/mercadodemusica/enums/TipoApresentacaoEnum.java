package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum TipoApresentacaoEnum implements Serializable{
	FOTO("Foto"),
	VIDEO("Video"),
	AUDIO("Audio");
	
	private String descricao;

	private TipoApresentacaoEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Enum<TipoApresentacaoEnum>[] getValues() {
		return TipoApresentacaoEnum.values();
	}
}
