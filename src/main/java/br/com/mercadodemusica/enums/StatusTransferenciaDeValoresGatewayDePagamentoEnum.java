package br.com.mercadodemusica.enums;

public enum StatusTransferenciaDeValoresGatewayDePagamentoEnum {
	STARTED ("Iniciado"),
	REQUESTED ("Pedido enviado"),
	FILE_CREATED ("Arquivo criado"),
	COMPLETED ("Depositado na conta"),
	FAILED ("Ocorreu um erro"),
	REVERSED ("Revertido");
	
	private String descricao;
	
	private StatusTransferenciaDeValoresGatewayDePagamentoEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
}
