package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum TipoDeTelefoneEnum implements Serializable{
	CELULAR("Celular"),
	RESIDENCIA("Residencia"),
	TRABALHO("Trabalho");
	
	private String descricao;

	private TipoDeTelefoneEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public Enum<TipoDeTelefoneEnum>[] getValues() {
		return TipoDeTelefoneEnum.values();
	}
}
