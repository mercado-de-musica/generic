package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum TipoPagamentoEnum implements Serializable{
	BoletoBancario("Boleto bancario"),
	CartaoCredito("Cartao de credito"),
	RedirecionamentoLoja("Redirecionamento para loja");
//	CartaoDebito("Cartao de debito");
	
	private String descricao;

	private TipoPagamentoEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
}
