package br.com.mercadodemusica.enums;

public enum ModuloSistemaEnum {
	USUARIO("modulo-usuario"),
	PRODUTO("modulo-produto"),
	PESQUISA("modulo-pesquisa"),
	PEDIDO("modulo-pedido");

	private String descricaoModulo;
	
	
	ModuloSistemaEnum(String descricaoModulo) {
		this.descricaoModulo = descricaoModulo;
	}
	
	public String getDescricaoModulo() {
		return descricaoModulo;
	}
	
}
