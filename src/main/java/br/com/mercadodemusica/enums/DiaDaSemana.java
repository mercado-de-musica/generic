package br.com.mercadodemusica.enums;

public enum DiaDaSemana {
	DOMINGO(1, "SUN"), SEGUNDA(2, "MON"), TERCA(3, "TUE"), QUARTA(4, "WED"), QUINTA(5, "THU"), SEXTA(6,
			"FRI"), SABADO(7, "SAT");

	private Integer diaNumerico;
	private String descricaoCron;

	private DiaDaSemana(Integer diaNumerico, String descricaoCron) {
		this.diaNumerico = diaNumerico;
		this.descricaoCron = descricaoCron;
	}

	public Integer getDiaNumerico() {
		return diaNumerico;
	}

	public String getDescricaoCron() {
		return descricaoCron;
	}
}
