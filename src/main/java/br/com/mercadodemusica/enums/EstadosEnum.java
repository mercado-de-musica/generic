package br.com.mercadodemusica.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;



@JsonFormat(shape=Shape.OBJECT)	
public enum EstadosEnum implements Serializable {
	
	AC("AC", "Acre"),
	AL("AL", "Alagoas"),
	AP("AP", "Amapá"),
	AM("AM", "Amazonas"),
	BA("BA", "Bahia"),
	CE("CE", "Ceará"),
	DF("DF", "Distrito Federal"),
	ES("ES", "Espírito Santo"),
	GO("GO", "Goiás"),
	MA("MA", "Maranhão"),
	MT("MT", "Mato Grosso"),	
	MS("MS", "Mato Grosso do Sul"),
	MG("MG", "Minas Gerais"),
	PA("PA", "Pará"),
	PB("PB", "Paraíba"),
	PR("PR", "Paraná"),
	PE("PE", "Pernambuco"),
	PI("PI", "Piauí"),
	RJ("RJ", "Rio de Janeiro"),
	RN("RN", "Rio Grande do Norte"),
	RS("RS", "Rio Grande do Sul"),
	RO("RO", "Rondônia"),
	RR("RR", "Roraima"),
	SC("SC", "Santa Catarina"),
	SE("SE", "Sergipe"),
	SP("SP", "São Paulo"),
	TO("TO", "Tocantins");
	
	private String sigla;
	private String descricao;

	private EstadosEnum(String sigla, String descricao) {
		this.sigla = sigla;
		this.descricao = descricao;
	}  
	
	public String getSigla() {
		return this.sigla;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
}
