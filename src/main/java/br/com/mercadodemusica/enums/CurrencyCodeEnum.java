package br.com.mercadodemusica.enums;

public enum CurrencyCodeEnum {
	BRL("Brasil");
	
	private String descricao;

	private CurrencyCodeEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
}
