package br.com.mercadodemusica.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TipoDtoUsuarioEnum {
	@JsonProperty("USUARIO_PESSOA_FISICA")
	USUARIO_PESSOA_FISICA("br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO"),
	@JsonProperty("USUARIO_PESSOA_JURIDICA")
	USUARIO_PESSOA_JURIDICA("br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO"),
	@JsonProperty("USUARIO_CRAWLER")
	USUARIO_CRAWLER("br.com.mercadodemusica.dto.UsuarioCrawlerDTO");

	private String nomeClasse;
	
	TipoDtoUsuarioEnum(String nomeClasse) {
		this.nomeClasse = nomeClasse;
	}
	
	public String getNomeClasse() {
		return nomeClasse;
	}
}
