package br.com.mercadodemusica.enums;

public enum LinguaEnum {
	pt_BR("Brasil");
	
	
	private String descricao;

	private LinguaEnum(String descricao) {
		this.descricao = descricao;
	}  
	
	public String getDescricao() {
		return this.descricao;
	}
}
