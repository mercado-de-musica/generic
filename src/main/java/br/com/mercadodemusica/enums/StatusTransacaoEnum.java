package br.com.mercadodemusica.enums;

import java.io.Serializable;

public enum StatusTransacaoEnum implements Serializable{
	AGUARDANDO_PAGAMENTO ("4", "Aguardando Pagamento"),
	EM_PROCESSAMENTO ("5", "Em Processamento"),
	APROVADO ("6", "Aprovado"),
	CANCELADO ("7", "Cancelado"),
	EM_CONTESTACAO ("24", "Em Contestação"),
	EM_MONITORAMENTO ("87", "Em Monitoramento"),
	EM_RECUPERACAO ("88", "Em Recuperação"),
	REPROVADO ("89", "Reprovado");
	
	private String numero;
	private String descricaoPortugues;
	
	private StatusTransacaoEnum(String numero, String descricaoPortugues) {
		this.numero = numero;
		this.descricaoPortugues = descricaoPortugues;
	}  
	
	public String getNumero() {
		return this.numero;
	}
	
	public String getDescricaoPortugues() {
		return this.descricaoPortugues;
	}
}
