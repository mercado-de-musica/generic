package br.com.mercadodemusica.enums;

import java.rmi.RemoteException;

public enum BancoEnum {
	_000("000", "Escolha aqui o banco"),
	_001("001", "BANCO DO BRASIL S.A."),
	_237("237", "BANCO BRADESCO S.A."),
	_341("341", "BANCO ITAU S.A."),
	_356("356", "BANCO ABN AMRO REAL S.A."),
	_409("409", "UNIBANCO UNIAO DE BANCOS BRASILEIROS S.A. (Unibanco)"),
	_041("041", "BANCO DO ESTADO DO RIO GRANDE DO SUL S.A. (Banrisul)"),
	_104("104", "CAIXA ECONOMICA FEDERAL"),
	_033("033", "BANCO SANTANDER S.A."),
	_399("399", "HSBC BANK BRASIL S.A.BANCO MULTIPLO"),
	_151("151", "BANCO NOSSA CAIXA S.A"),
	_389("389", "BANCO MERCANTIL DO BRASIL S.A."),
	_004("004", "BANCO DO NORDESTE DO BRASIL S.A"),
	_021("021", "BANESTES S.A BANCO DO ESTADO DO ESPIRITO SANTO"),
	_477("477", "CITIBANK N.A."),
	_422("422", "BANCO SAFRA S.A."),
	_003("003", "BANCO DA AMAZONIA S.A."),
	_047("047", "Banco do Estado de Sergipe S.A"),
	_070("070", "Banco de Brasília S.A."),
	_655("655", "Banco Votorantim S.A"),
	_107("107", "Banco BBM S.A."),
	_025("025", "Banco Alfa S.A."),
	_263("263", "Banco Cacique S.A."),
	_229("229", "BANCO CRUZEIRO DO SUL S.A."),
	_252("252", "BANCO FININVEST S.A."),
	_063("063", "BANCO IBI S.A - BANCO MULTIPLO"),
	_623("623", "BANCO PANAMERICANO S.A."),
	_633("633", "BANCO RENDIMENTO S.A."),
	_749("749", "BANCO SIMPLES S.A."),
	_215("215", "BANCO ACOMERCIAL E DE INVESTIMENTO SUDAMERIS S.A."),
	_756("756", "BANCO COOPERATIVO DO BRASIL S.A. - (BANCOOB)"),
	_748("748", "BANCO COOPERATIVO SICREDI S.A."),
	_065("065", "LEMON BANK BANCO MÚLTIPLO S.A."),
	_069("069", "BPN BRASIL BANCO MÚLTIPLO S.A."),
	_719("719", "BANIF - BANCO INTERNACIONAL DO FUNCHAL (BRASIL), S.A. (Banif)"),
	_318("318", "BANCO BMG S.A."),
	_037("037", "BANCO DO ESTADO DO PARA S.A."),
	_745("745", "BANCO CITIBANK S.A.");
	
	private String numero;
	private String descricao;

	private BancoEnum(String numero, String descricao) {
		this.numero = numero;
		this.descricao = descricao;
	}  
	
	public String getNumero() throws RemoteException {
		return this.numero;
	}
	
	public String getDescricao() throws RemoteException {
		return this.descricao;
	}
}
