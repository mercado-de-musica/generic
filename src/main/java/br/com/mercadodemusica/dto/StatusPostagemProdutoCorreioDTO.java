package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.mercadodemusica.enums.StatusPostagemCorreiosEnum;

public class StatusPostagemProdutoCorreioDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private StatusPostagemCorreiosEnum status;
	
	private Calendar dataDoProcessamento;
	
	private Boolean ativo;
	
	@JsonBackReference
	private RastreamentoDTO rastreamentoProdutoCorreios;
	

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}
	
	public StatusPostagemCorreiosEnum getStatus() {
		return status;
	}

	public void setStatus(StatusPostagemCorreiosEnum status) {
		this.status = status;
	}

	public Calendar getDataDoProcessamento() {
		return dataDoProcessamento;
	}

	public void setDataDoProcessamento(Calendar dataDoProcessamento) {
		this.dataDoProcessamento = dataDoProcessamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public RastreamentoDTO getRastreamentoProdutoCorreios() {
		return rastreamentoProdutoCorreios;
	}

	public void setRastreamentoProdutoCorreios(RastreamentoDTO rastreamentoProdutoCorreios) {
		this.rastreamentoProdutoCorreios = rastreamentoProdutoCorreios;
	}
}
