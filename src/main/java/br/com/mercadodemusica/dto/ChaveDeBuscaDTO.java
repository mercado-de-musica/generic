package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ChaveDeBuscaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private String chaveDeBusca;

	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getChaveDeBusca() {
		return chaveDeBusca;
	}

	public void setChaveDeBusca(String chaveDeBusca) {
		this.chaveDeBusca = chaveDeBusca;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

}
