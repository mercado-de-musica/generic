package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class DesistenciaProdutoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2317608489166825337L;
	
	private BigInteger id;
	
	private RastreamentoDTO rastreamento;
	
	private Calendar dataDoProcessamento;
	
	private Boolean finalizado = Boolean.FALSE;
	
	private String protocoloDesistenciaDeCompra;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public RastreamentoDTO getRastreamento() {
		return rastreamento;
	}

	public void setRastreamento(RastreamentoDTO rastreamento) {
		this.rastreamento = rastreamento;
	}

	public Calendar getDataDoProcessamento() {
		return dataDoProcessamento;
	}

	public void setDataDoProcessamento(Calendar dataDoProcessamento) {
		this.dataDoProcessamento = dataDoProcessamento;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getProtocoloDesistenciaDeCompra() {
		return protocoloDesistenciaDeCompra;
	}

	public void setProtocoloDesistenciaDeCompra(String protocoloDesistenciaDeCompra) {
		this.protocoloDesistenciaDeCompra = protocoloDesistenciaDeCompra;
	}
}
