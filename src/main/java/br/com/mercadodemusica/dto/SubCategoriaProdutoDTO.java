package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class SubCategoriaProdutoDTO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private SubCategoriaDTO subcategoria;
	
	private ProdutoDTO produto;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public SubCategoriaDTO getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(SubCategoriaDTO subcategoria) {
		this.subcategoria = subcategoria;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}
}
