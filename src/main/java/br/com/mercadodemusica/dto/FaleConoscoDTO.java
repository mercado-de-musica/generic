package br.com.mercadodemusica.dto;

import java.io.Serializable;

public class FaleConoscoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;
	
	private String sobrenome;
	
	private String email;
	
	private String mensagem;
	
	private String tipoDeMensagem;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTipoDeMensagem() {
		return tipoDeMensagem;
	}

	public void setTipoDeMensagem(String tipoDeMensagem) {
		this.tipoDeMensagem = tipoDeMensagem;
	}
}
