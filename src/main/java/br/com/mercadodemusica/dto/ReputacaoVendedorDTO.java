package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ReputacaoVendedorDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private BigInteger idProduto;
	
//	@JsonBackReference(value="usuarioVendedor")
	private UsuarioDTO usuarioVendedor;
	
	private UsuarioDTO usuarioComprador;
	
	private String mensagem;
	
	private Integer nota;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(BigInteger idProduto) {
		this.idProduto = idProduto;
	}

	public UsuarioDTO getUsuarioVendedor() {
		return usuarioVendedor;
	}

	public void setUsuarioVendedor(UsuarioDTO usuarioVendedor) {
		this.usuarioVendedor = usuarioVendedor;
	}

	public UsuarioDTO getUsuarioComprador() {
		return usuarioComprador;
	}

	public void setUsuarioComprador(UsuarioDTO usuarioComprador) {
		this.usuarioComprador = usuarioComprador;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}
}
