package br.com.mercadodemusica.dto;

import java.io.Serializable;

import br.com.mercadodemusica.enums.TipoDeRequisicaoDoSiteEnum;

public class UsuarioCrawlerDTO extends UsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;

	private String url;

	private TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum;
	

//	@JsonBackReference(value = "palavrasChavesSites")
//	private List<PalavraChaveSiteDTO> palavrasChavesSites;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TipoDeRequisicaoDoSiteEnum getTipoDeProcuraDoSiteEnum() {
		return tipoDeProcuraDoSiteEnum;
	}

	public void setTipoDeProcuraDoSiteEnum(TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum) {
		this.tipoDeProcuraDoSiteEnum = tipoDeProcuraDoSiteEnum;
	}

//	public List<PalavraChaveSiteDTO> getPalavrasChavesSites() {
//		return palavrasChavesSites;
//	}
//
//	public void setPalavrasChavesSites(List<PalavraChaveSiteDTO> palavrasChavesSites) {
//		this.palavrasChavesSites = palavrasChavesSites;
//	}
}
