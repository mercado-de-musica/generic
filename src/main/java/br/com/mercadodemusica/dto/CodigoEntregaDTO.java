package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class CodigoEntregaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6901777476105905902L;

	private BigInteger id;
	
	private String codigoComprador;
	
	private String codigoVendedor;
	
	private Calendar dataGeracaoCodigo;
	
	private Boolean codigoUtilizado = Boolean.FALSE;
	
	private String codigoInseridoComprador;
	
	private String codigoInseridoVendedor;
	
	private Calendar dataInsercaoCodigoComprador;
	
	private Calendar dataInsercaoCodigoVendedor;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCodigoComprador() {
		return codigoComprador;
	}

	public void setCodigoComprador(String codigoComprador) {
		this.codigoComprador = codigoComprador;
	}

	public String getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public Calendar getDataGeracaoCodigo() {
		return dataGeracaoCodigo;
	}

	public void setDataGeracaoCodigo(Calendar dataGeracaoCodigo) {
		this.dataGeracaoCodigo = dataGeracaoCodigo;
	}

	public Boolean getCodigoUtilizado() {
		return codigoUtilizado;
	}

	public void setCodigoUtilizado(Boolean codigoUtilizado) {
		this.codigoUtilizado = codigoUtilizado;
	}

	public String getCodigoInseridoComprador() {
		return codigoInseridoComprador;
	}

	public void setCodigoInseridoComprador(String codigoInseridoComprador) {
		this.codigoInseridoComprador = codigoInseridoComprador;
	}

	public String getCodigoInseridoVendedor() {
		return codigoInseridoVendedor;
	}

	public void setCodigoInseridoVendedor(String codigoInseridoVendedor) {
		this.codigoInseridoVendedor = codigoInseridoVendedor;
	}

	public Calendar getDataInsercaoCodigoComprador() {
		return dataInsercaoCodigoComprador;
	}

	public void setDataInsercaoCodigoComprador(Calendar dataInsercaoCodigoComprador) {
		this.dataInsercaoCodigoComprador = dataInsercaoCodigoComprador;
	}

	public Calendar getDataInsercaoCodigoVendedor() {
		return dataInsercaoCodigoVendedor;
	}

	public void setDataInsercaoCodigoVendedor(Calendar dataInsercaoCodigoVendedor) {
		this.dataInsercaoCodigoVendedor = dataInsercaoCodigoVendedor;
	}
	
}
