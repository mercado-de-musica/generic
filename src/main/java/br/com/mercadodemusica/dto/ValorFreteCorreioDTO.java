package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class ValorFreteCorreioDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2220929329942561335L;

	private BigInteger id;
	
	private BigDecimal valor;
	
	private Long idServicoCorreio;
	
	private String codigoCorreio;
	
	private String descricaoCorreio;
	
	@JsonBackReference
	private ProdutoDTO infosGeraisProdutoUsuario;

	private String diasEntrega;
	
	private Boolean ativoParaCompra;
	
	private Calendar dataAtualizacao;
	
	/*
	 * atributo utilizado para fazer os merges de dados apenas
	 */
	
	private Boolean ativoMerge;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Long getIdServicoCorreio() {
		return idServicoCorreio;
	}

	public void setIdServicoCorreio(Long idServicoCorreio) {
		this.idServicoCorreio = idServicoCorreio;
	}

	public String getCodigoCorreio() {
		return codigoCorreio;
	}

	public void setCodigoCorreio(String codigoCorreio) {
		this.codigoCorreio = codigoCorreio;
	}

	public String getDescricaoCorreio() {
		return descricaoCorreio;
	}

	public void setDescricaoCorreio(String descricaoCorreio) {
		this.descricaoCorreio = descricaoCorreio;
	}

	public ProdutoDTO getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(ProdutoDTO infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public String getDiasEntrega() {
		return diasEntrega;
	}

	public void setDiasEntrega(String diasEntrega) {
		this.diasEntrega = diasEntrega;
	}

	public Boolean getAtivoParaCompra() {
		return ativoParaCompra;
	}

	public void setAtivoParaCompra(Boolean ativoParaCompra) {
		this.ativoParaCompra = ativoParaCompra;
	}

	public Calendar getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Calendar dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Boolean getAtivoMerge() {
		return ativoMerge;
	}

	public void setAtivoMerge(Boolean ativoMerge) {
		this.ativoMerge = ativoMerge;
	}
}
