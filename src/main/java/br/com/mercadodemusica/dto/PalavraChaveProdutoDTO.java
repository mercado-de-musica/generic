package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class PalavraChaveProdutoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private BigInteger id;
	
	private PalavraChaveDTO palavraChave;
	
	private ProdutoDTO produto;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PalavraChaveDTO getPalavraChave() {
		return palavraChave;
	}

	public void setPalavraChave(PalavraChaveDTO palavraChave) {
		this.palavraChave = palavraChave;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}
}
