package br.com.mercadodemusica.dto;

import java.math.BigInteger;

public class PalavraProibidaDTO {

	private BigInteger id;

	private String palavra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getPalavra() {
		return palavra;
	}

	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}

}
