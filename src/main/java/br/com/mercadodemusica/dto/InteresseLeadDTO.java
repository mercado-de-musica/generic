package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class InteresseLeadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private String nome;
	
	private LeadDTO lead;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LeadDTO getLead() {
		return lead;
	}

	public void setLead(LeadDTO lead) {
		this.lead = lead;
	}
}
