package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class PalavraChaveUsuarioDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private PalavraChaveDTO palavraChave;
	
//	@JsonManagedReference(value="site")
	private UsuarioDTO usuario;
	
	private Boolean buscouProduto = Boolean.FALSE;
	
	private Boolean ativo = Boolean.TRUE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PalavraChaveDTO getPalavraChave() {
		return palavraChave;
	}

	public void setPalavraChave(PalavraChaveDTO palavraChave) {
		this.palavraChave = palavraChave;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public Boolean getBuscouProduto() {
		return buscouProduto;
	}

	public void setBuscouProduto(Boolean buscouProduto) {
		this.buscouProduto = buscouProduto;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
