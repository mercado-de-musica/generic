package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class TokenLeadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private LeadDTO lead;
	
	private String token;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public LeadDTO getLead() {
		return lead;
	}

	public void setLead(LeadDTO lead) {
		this.lead = lead;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
