package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import br.com.mercadodemusica.enums.FinalizacaoTask;

public class LogTaskDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private FinalizacaoTask finalizacaoTask;

	private String descricaoFinalizacaoTask;

	private Calendar dataFinalizacao;

	private TaskScheduladaDTO taskSchedulada;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public FinalizacaoTask getFinalizacaoTask() {
		return finalizacaoTask;
	}

	public void setFinalizacaoTask(FinalizacaoTask finalizacaoTask) {
		this.finalizacaoTask = finalizacaoTask;
	}

	public String getDescricaoFinalizacaoTask() {
		return descricaoFinalizacaoTask;
	}

	public void setDescricaoFinalizacaoTask(String descricaoFinalizacaoTask) {
		this.descricaoFinalizacaoTask = descricaoFinalizacaoTask;
	}

	public Calendar getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Calendar dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public TaskScheduladaDTO getTaskSchedulada() {
		return taskSchedulada;
	}

	public void setTaskSchedulada(TaskScheduladaDTO taskSchedulada) {
		this.taskSchedulada = taskSchedulada;
	}
}
