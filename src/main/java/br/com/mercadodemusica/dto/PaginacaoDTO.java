package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class PaginacaoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private String urlPaginacao;

	private PalavraChaveDTO palavraChave;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUrlPaginacao() {
		return urlPaginacao;
	}

	public void setUrlPaginacao(String urlPaginacao) {
		this.urlPaginacao = urlPaginacao;
	}

	public PalavraChaveDTO getPalavraChave() {
		return palavraChave;
	}

	public void setPalavraChave(PalavraChaveDTO palavraChave) {
		this.palavraChave = palavraChave;
	}
}
