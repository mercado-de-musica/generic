package br.com.mercadodemusica.dto;

import java.io.Serializable;

import br.com.mercadodemusica.enums.DiaDaSemana;
import br.com.mercadodemusica.enums.MotivoTagEnum;
import br.com.mercadodemusica.enums.TipoDeParametroEnum;
import br.com.mercadodemusica.enums.TipoDeRequisicaoDoSiteEnum;
import br.com.mercadodemusica.enums.TipoTagEnum;

public class InfosIniciaisCrawlerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4913950269451826552L;

	private String site;

	private String urlSite;

	private TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum;

	private String tagNaoEncontrada;

	private TipoTagEnum tipoTagNaoEncontrada;

	private MotivoTagEnum motivoTagNaoEncontrada;

	private String tagProcura;

	private TipoTagEnum tipoTagProcura;

	private MotivoTagEnum motivoTagProcura;

	private String palavra;

	private String chaveDeBusca;

	private String tagListaDeProdutos;

	private TipoTagEnum tipoTagListaDeProdutos;

	private MotivoTagEnum motivoTagListaDeProdutos;

	private String tagPaginacao;

	private TipoTagEnum tipoTagPaginacao;

	private MotivoTagEnum motivoTagPaginacao;

	private String parametrosPaginacaoUrl;

	private TipoDeParametroEnum parametrosPaginacaoTipoParametro;

	private String parametrosPesquisaUrl;

	private TipoDeParametroEnum parametrosPesquisaTipoParametro;
	
	
	
	private String tagDetalheProduto;

	private TipoTagEnum tipoTagDetalheProduto;

	private MotivoTagEnum motivoTagDetalheProduto;
	
	
	
	private String tagNomeProduto;

	private TipoTagEnum tipoTagNomeProduto;

	private MotivoTagEnum motivoTagNomeProduto;
	
	private String tagInformacoesProduto;

	private TipoTagEnum tipoTagInformacoesProduto;

	private MotivoTagEnum motivoTagInformacoesProduto;
	
	private String tagPreco;

	private TipoTagEnum tipoTagPreco;

	private MotivoTagEnum motivoTagPreco;
	
	
	
	
	private String tagFotoProduto;

	private TipoTagEnum tipoTagFotoProduto;

	private MotivoTagEnum motivoTagFotoProduto;
	
	
	
	
	private String tagImagemSite;
	
	private String fundoBranco;

	private TipoTagEnum tipoTagImagemSite;

	private MotivoTagEnum motivoTagImagemSite;
	
	
	
	private String tagProdutoIndisponivel;

	private TipoTagEnum tipoTagProdutoIndisponivel;

	private MotivoTagEnum motivoTagProdutoIndisponivel;
	
	private String atributoHtml;

	private DiaDaSemana diaDaSemana;
	
	private String horaString;
	
	private String minutoString;

	
	public String getSite() {
		return site;
	}
	
	public void setSite(String site) {
		this.site = site;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public TipoDeRequisicaoDoSiteEnum getTipoDeProcuraDoSiteEnum() {
		return tipoDeProcuraDoSiteEnum;
	}

	public void setTipoDeProcuraDoSiteEnum(TipoDeRequisicaoDoSiteEnum tipoDeProcuraDoSiteEnum) {
		this.tipoDeProcuraDoSiteEnum = tipoDeProcuraDoSiteEnum;
	}

	public String getTagNaoEncontrada() {
		return tagNaoEncontrada;
	}

	public void setTagNaoEncontrada(String tagNaoEncontrada) {
		this.tagNaoEncontrada = tagNaoEncontrada;
	}

	public TipoTagEnum getTipoTagNaoEncontrada() {
		return tipoTagNaoEncontrada;
	}

	public void setTipoTagNaoEncontrada(TipoTagEnum tipoTagNaoEncontrada) {
		this.tipoTagNaoEncontrada = tipoTagNaoEncontrada;
	}

	public MotivoTagEnum getMotivoTagNaoEncontrada() {
		return motivoTagNaoEncontrada;
	}

	public void setMotivoTagNaoEncontrada(MotivoTagEnum motivoTagNaoEncontrada) {
		this.motivoTagNaoEncontrada = motivoTagNaoEncontrada;
	}

	public String getTagProcura() {
		return tagProcura;
	}

	public void setTagProcura(String tagProcura) {
		this.tagProcura = tagProcura;
	}

	public TipoTagEnum getTipoTagProcura() {
		return tipoTagProcura;
	}

	public void setTipoTagProcura(TipoTagEnum tipoTagProcura) {
		this.tipoTagProcura = tipoTagProcura;
	}

	public MotivoTagEnum getMotivoTagProcura() {
		return motivoTagProcura;
	}

	public void setMotivoTagProcura(MotivoTagEnum motivoTagProcura) {
		this.motivoTagProcura = motivoTagProcura;
	}

	public String getPalavra() {
		return palavra;
	}

	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}

	public String getChaveDeBusca() {
		return chaveDeBusca;
	}

	public void setChaveDeBusca(String chaveDeBusca) {
		this.chaveDeBusca = chaveDeBusca;
	}

	public String getTagListaDeProdutos() {
		return tagListaDeProdutos;
	}

	public void setTagListaDeProdutos(String tagListaDeProdutos) {
		this.tagListaDeProdutos = tagListaDeProdutos;
	}

	public TipoTagEnum getTipoTagListaDeProdutos() {
		return tipoTagListaDeProdutos;
	}

	public void setTipoTagListaDeProdutos(TipoTagEnum tipoTagListaDeProdutos) {
		this.tipoTagListaDeProdutos = tipoTagListaDeProdutos;
	}

	public MotivoTagEnum getMotivoTagListaDeProdutos() {
		return motivoTagListaDeProdutos;
	}

	public void setMotivoTagListaDeProdutos(MotivoTagEnum motivoTagListaDeProdutos) {
		this.motivoTagListaDeProdutos = motivoTagListaDeProdutos;
	}

	public String getTagPaginacao() {
		return tagPaginacao;
	}

	public void setTagPaginacao(String tagPaginacao) {
		this.tagPaginacao = tagPaginacao;
	}

	public TipoTagEnum getTipoTagPaginacao() {
		return tipoTagPaginacao;
	}

	public void setTipoTagPaginacao(TipoTagEnum tipoTagPaginacao) {
		this.tipoTagPaginacao = tipoTagPaginacao;
	}

	public MotivoTagEnum getMotivoTagPaginacao() {
		return motivoTagPaginacao;
	}

	public void setMotivoTagPaginacao(MotivoTagEnum motivoTagPaginacao) {
		this.motivoTagPaginacao = motivoTagPaginacao;
	}

	public String getParametrosPaginacaoUrl() {
		return parametrosPaginacaoUrl;
	}

	public void setParametrosPaginacaoUrl(String parametrosPaginacaoUrl) {
		this.parametrosPaginacaoUrl = parametrosPaginacaoUrl;
	}

	public TipoDeParametroEnum getParametrosPaginacaoTipoParametro() {
		return parametrosPaginacaoTipoParametro;
	}

	public void setParametrosPaginacaoTipoParametro(TipoDeParametroEnum parametrosPaginacaoTipoParametro) {
		this.parametrosPaginacaoTipoParametro = parametrosPaginacaoTipoParametro;
	}

	public String getParametrosPesquisaUrl() {
		return parametrosPesquisaUrl;
	}

	public void setParametrosPesquisaUrl(String parametrosPesquisaUrl) {
		this.parametrosPesquisaUrl = parametrosPesquisaUrl;
	}

	public TipoDeParametroEnum getParametrosPesquisaTipoParametro() {
		return parametrosPesquisaTipoParametro;
	}

	public void setParametrosPesquisaTipoParametro(TipoDeParametroEnum parametrosPesquisaTipoParametro) {
		this.parametrosPesquisaTipoParametro = parametrosPesquisaTipoParametro;
	}

	public String getTagDetalheProduto() {
		return tagDetalheProduto;
	}

	public void setTagDetalheProduto(String tagDetalheProduto) {
		this.tagDetalheProduto = tagDetalheProduto;
	}

	public TipoTagEnum getTipoTagDetalheProduto() {
		return tipoTagDetalheProduto;
	}

	public void setTipoTagDetalheProduto(TipoTagEnum tipoTagDetalheProduto) {
		this.tipoTagDetalheProduto = tipoTagDetalheProduto;
	}

	public MotivoTagEnum getMotivoTagDetalheProduto() {
		return motivoTagDetalheProduto;
	}

	public void setMotivoTagDetalheProduto(MotivoTagEnum motivoTagDetalheProduto) {
		this.motivoTagDetalheProduto = motivoTagDetalheProduto;
	}

	public String getTagNomeProduto() {
		return tagNomeProduto;
	}

	public void setTagNomeProduto(String tagNomeProduto) {
		this.tagNomeProduto = tagNomeProduto;
	}

	public TipoTagEnum getTipoTagNomeProduto() {
		return tipoTagNomeProduto;
	}

	public void setTipoTagNomeProduto(TipoTagEnum tipoTagNomeProduto) {
		this.tipoTagNomeProduto = tipoTagNomeProduto;
	}

	public MotivoTagEnum getMotivoTagNomeProduto() {
		return motivoTagNomeProduto;
	}

	public void setMotivoTagNomeProduto(MotivoTagEnum motivoTagNomeProduto) {
		this.motivoTagNomeProduto = motivoTagNomeProduto;
	}

	public String getTagInformacoesProduto() {
		return tagInformacoesProduto;
	}

	public void setTagInformacoesProduto(String tagInformacoesProduto) {
		this.tagInformacoesProduto = tagInformacoesProduto;
	}

	public TipoTagEnum getTipoTagInformacoesProduto() {
		return tipoTagInformacoesProduto;
	}

	public void setTipoTagInformacoesProduto(TipoTagEnum tipoTagInformacoesProduto) {
		this.tipoTagInformacoesProduto = tipoTagInformacoesProduto;
	}

	public MotivoTagEnum getMotivoTagInformacoesProduto() {
		return motivoTagInformacoesProduto;
	}

	public void setMotivoTagInformacoesProduto(MotivoTagEnum motivoTagInformacoesProduto) {
		this.motivoTagInformacoesProduto = motivoTagInformacoesProduto;
	}

	public String getTagPreco() {
		return tagPreco;
	}

	public void setTagPreco(String tagPreco) {
		this.tagPreco = tagPreco;
	}

	public TipoTagEnum getTipoTagPreco() {
		return tipoTagPreco;
	}

	public void setTipoTagPreco(TipoTagEnum tipoTagPreco) {
		this.tipoTagPreco = tipoTagPreco;
	}

	public MotivoTagEnum getMotivoTagPreco() {
		return motivoTagPreco;
	}

	public void setMotivoTagPreco(MotivoTagEnum motivoTagPreco) {
		this.motivoTagPreco = motivoTagPreco;
	}

	public String getTagFotoProduto() {
		return tagFotoProduto;
	}

	public void setTagFotoProduto(String tagFotoProduto) {
		this.tagFotoProduto = tagFotoProduto;
	}

	public TipoTagEnum getTipoTagFotoProduto() {
		return tipoTagFotoProduto;
	}

	public void setTipoTagFotoProduto(TipoTagEnum tipoTagFotoProduto) {
		this.tipoTagFotoProduto = tipoTagFotoProduto;
	}

	public MotivoTagEnum getMotivoTagFotoProduto() {
		return motivoTagFotoProduto;
	}

	public void setMotivoTagFotoProduto(MotivoTagEnum motivoTagFotoProduto) {
		this.motivoTagFotoProduto = motivoTagFotoProduto;
	}

	public String getTagProdutoIndisponivel() {
		return tagProdutoIndisponivel;
	}

	public void setTagProdutoIndisponivel(String tagProdutoIndisponivel) {
		this.tagProdutoIndisponivel = tagProdutoIndisponivel;
	}

	public TipoTagEnum getTipoTagProdutoIndisponivel() {
		return tipoTagProdutoIndisponivel;
	}

	public void setTipoTagProdutoIndisponivel(TipoTagEnum tipoTagProdutoIndisponivel) {
		this.tipoTagProdutoIndisponivel = tipoTagProdutoIndisponivel;
	}

	public MotivoTagEnum getMotivoTagProdutoIndisponivel() {
		return motivoTagProdutoIndisponivel;
	}

	public void setMotivoTagProdutoIndisponivel(MotivoTagEnum motivoTagProdutoIndisponivel) {
		this.motivoTagProdutoIndisponivel = motivoTagProdutoIndisponivel;
	}

	public String getAtributoHtml() {
		return atributoHtml;
	}

	public void setAtributoHtml(String atributoHtml) {
		this.atributoHtml = atributoHtml;
	}

	public DiaDaSemana getDiaDaSemana() {
		return diaDaSemana;
	}

	public void setDiaDaSemana(DiaDaSemana diaDaSemana) {
		this.diaDaSemana = diaDaSemana;
	}

	public String getHoraString() {
		return horaString;
	}

	public void setHoraString(String horaString) {
		this.horaString = horaString;
	}

	public String getMinutoString() {
		return minutoString;
	}

	public void setMinutoString(String minutoString) {
		this.minutoString = minutoString;
	}

	public String getTagImagemSite() {
		return tagImagemSite;
	}

	public void setTagImagemSite(String tagImagemSite) {
		this.tagImagemSite = tagImagemSite;
	}

	public String getFundoBranco() {
		return fundoBranco;
	}

	public void setFundoBranco(String fundoBranco) {
		this.fundoBranco = fundoBranco;
	}

	public TipoTagEnum getTipoTagImagemSite() {
		return tipoTagImagemSite;
	}

	public void setTipoTagImagemSite(TipoTagEnum tipoTagImagemSite) {
		this.tipoTagImagemSite = tipoTagImagemSite;
	}

	public MotivoTagEnum getMotivoTagImagemSite() {
		return motivoTagImagemSite;
	}

	public void setMotivoTagImagemSite(MotivoTagEnum motivoTagImagemSite) {
		this.motivoTagImagemSite = motivoTagImagemSite;
	}
}
