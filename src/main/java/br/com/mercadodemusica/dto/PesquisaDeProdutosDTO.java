package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import br.com.mercadodemusica.enums.TipoPesquisa;

public class PesquisaDeProdutosDTO implements Serializable {
	
	/**
	 * DTO USADO PARA TRAZER O RESULTADO DAS PESQUISAS REALIZADAS E TAMBEM O DETALHAMENTO DO PRODUTO
	 * 
	 * 
	 */
	private static final long serialVersionUID = 7735877946549930214L;

	private List<CategoriaDTO> listaDeCategoriasDto;
	
	private List<SubCategoriaDTO> listaDeSubcategoriasEscolhidas;
	
	private List<ApresentacaoProdutoDTO> listaApresentacaoProduto;
	
	private List<FotoCrawlerDTO> listaApresentacaoProdutoCrawler;
	
	private ApresentacaoProdutoDTO apresentacaoProdutoCapa;
	
	private BigDecimal maiorValorDosProdutos;
	
	private BigDecimal menorValorDosProdutos;
	
	private BigDecimal primeiroValorDosProdutos;
	
	private BigDecimal ultimoValorDosProdutos;
	
	private String maiorValorDosProdutosString;
	
	private String menorValorDosProdutosString;
	
	private CompraDTO compra;
	
	private Boolean verificacaoMetodoDeAcesso;
	
	private String h3titulo;
	
	private BigInteger contagem;
	
	private BigInteger paginas;
	
	private BigInteger produtosPorPagina;
	
	private BigInteger instrumentoAcessorioIdProcura;
	
	private BigInteger marcaIdProcura;
	
	private BigInteger produtoIdProcura;
	
	private BigInteger modeloIdProcura;
	
	private String reputacaoVendedor;

	private BigInteger paginaAtiva;
	
	private TipoPesquisa tipoPesquisa;
	
	private String nomeVendedor;
	
	private Boolean produtosEncontrados;

	public List<CategoriaDTO> getListaDeCategoriasDto() {
		return listaDeCategoriasDto;
	}

	public void setListaDeCategoriasDto(List<CategoriaDTO> listaDeCategoriasDto) {
		this.listaDeCategoriasDto = listaDeCategoriasDto;
	}

	public List<SubCategoriaDTO> getListaDeSubcategoriasEscolhidas() {
		return listaDeSubcategoriasEscolhidas;
	}

	public void setListaDeSubcategoriasEscolhidas(List<SubCategoriaDTO> listaDeSubcategoriasEscolhidas) {
		this.listaDeSubcategoriasEscolhidas = listaDeSubcategoriasEscolhidas;
	}

	public List<ApresentacaoProdutoDTO> getListaApresentacaoProduto() {
		return listaApresentacaoProduto;
	}

	public void setListaApresentacaoProduto(List<ApresentacaoProdutoDTO> listaApresentacaoProduto) {
		this.listaApresentacaoProduto = listaApresentacaoProduto;
	}

	public List<FotoCrawlerDTO> getListaApresentacaoProdutoCrawler() {
		return listaApresentacaoProdutoCrawler;
	}

	public void setListaApresentacaoProdutoCrawler(List<FotoCrawlerDTO> listaApresentacaoProdutoCrawler) {
		this.listaApresentacaoProdutoCrawler = listaApresentacaoProdutoCrawler;
	}

	public ApresentacaoProdutoDTO getApresentacaoProdutoCapa() {
		return apresentacaoProdutoCapa;
	}

	public void setApresentacaoProdutoCapa(ApresentacaoProdutoDTO apresentacaoProdutoCapa) {
		this.apresentacaoProdutoCapa = apresentacaoProdutoCapa;
	}

	public BigDecimal getMaiorValorDosProdutos() {
		return maiorValorDosProdutos;
	}

	public void setMaiorValorDosProdutos(BigDecimal maiorValorDosProdutos) {
		this.maiorValorDosProdutos = maiorValorDosProdutos;
	}

	public BigDecimal getMenorValorDosProdutos() {
		return menorValorDosProdutos;
	}

	public void setMenorValorDosProdutos(BigDecimal menorValorDosProdutos) {
		this.menorValorDosProdutos = menorValorDosProdutos;
	}

	public String getMaiorValorDosProdutosString() {
		return maiorValorDosProdutosString;
	}

	public void setMaiorValorDosProdutosString(String maiorValorDosProdutosString) {
		this.maiorValorDosProdutosString = maiorValorDosProdutosString;
	}

	public String getMenorValorDosProdutosString() {
		return menorValorDosProdutosString;
	}

	public void setMenorValorDosProdutosString(String menorValorDosProdutosString) {
		this.menorValorDosProdutosString = menorValorDosProdutosString;
	}

	public BigDecimal getPrimeiroValorDosProdutos() {
		return primeiroValorDosProdutos;
	}

	public void setPrimeiroValorDosProdutos(BigDecimal primeiroValorDosProdutos) {
		this.primeiroValorDosProdutos = primeiroValorDosProdutos;
	}

	public BigDecimal getUltimoValorDosProdutos() {
		return ultimoValorDosProdutos;
	}

	public void setUltimoValorDosProdutos(BigDecimal ultimoValorDosProdutos) {
		this.ultimoValorDosProdutos = ultimoValorDosProdutos;
	}

	public CompraDTO getCompra() {
		return compra;
	}

	public void setCompra(CompraDTO compra) {
		this.compra = compra;
	}

	public Boolean getVerificacaoMetodoDeAcesso() {
		return verificacaoMetodoDeAcesso;
	}

	public void setVerificacaoMetodoDeAcesso(Boolean verificacaoMetodoDeAcesso) {
		this.verificacaoMetodoDeAcesso = verificacaoMetodoDeAcesso;
	}

	public String getH3titulo() {
		return h3titulo;
	}

	public void setH3titulo(String h3titulo) {
		this.h3titulo = h3titulo;
	}

	public BigInteger getContagem() {
		return contagem;
	}

	public void setContagem(BigInteger contagem) {
		this.contagem = contagem;
	}

	public BigInteger getPaginas() {
		return paginas;
	}

	public void setPaginas(BigInteger paginas) {
		this.paginas = paginas;
	}

	public BigInteger getProdutosPorPagina() {
		return produtosPorPagina;
	}

	public void setProdutosPorPagina(BigInteger produtosPorPagina) {
		this.produtosPorPagina = produtosPorPagina;
	}

	public BigInteger getInstrumentoAcessorioIdProcura() {
		return instrumentoAcessorioIdProcura;
	}

	public void setInstrumentoAcessorioIdProcura(BigInteger instrumentoAcessorioIdProcura) {
		this.instrumentoAcessorioIdProcura = instrumentoAcessorioIdProcura;
	}

	public BigInteger getMarcaIdProcura() {
		return marcaIdProcura;
	}

	public void setMarcaIdProcura(BigInteger marcaIdProcura) {
		this.marcaIdProcura = marcaIdProcura;
	}

	public BigInteger getProdutoIdProcura() {
		return produtoIdProcura;
	}

	public void setProdutoIdProcura(BigInteger produtoIdProcura) {
		this.produtoIdProcura = produtoIdProcura;
	}

	public BigInteger getModeloIdProcura() {
		return modeloIdProcura;
	}

	public void setModeloIdProcura(BigInteger modeloIdProcura) {
		this.modeloIdProcura = modeloIdProcura;
	}

	public String getReputacaoVendedor() {
		return reputacaoVendedor;
	}

	public void setReputacaoVendedor(String reputacaoVendedor) {
		this.reputacaoVendedor = reputacaoVendedor;
	}

	public BigInteger getPaginaAtiva() {
		return paginaAtiva;
	}

	public void setPaginaAtiva(BigInteger paginaAtiva) {
		this.paginaAtiva = paginaAtiva;
	}

	public TipoPesquisa getTipoPesquisa() {
		return tipoPesquisa;
	}

	public void setTipoPesquisa(TipoPesquisa tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}

	public String getNomeVendedor() {
		return nomeVendedor;
	}

	public void setNomeVendedor(String nomeVendedor) {
		this.nomeVendedor = nomeVendedor;
	}

	public Boolean getProdutosEncontrados() {
		return produtosEncontrados;
	}

	public void setProdutosEncontrados(Boolean produtosEncontrados) {
		this.produtosEncontrados = produtosEncontrados;
	}
}
