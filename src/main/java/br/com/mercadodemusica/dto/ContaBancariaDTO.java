package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.BancoEnum;

public class ContaBancariaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2267414283373372356L;

	private BigInteger id;
	
	private BancoEnum numeroDoBanco;
	
	private String numeroDaAgencia;
	
	private String digitoDaAgencia;
	
	private String numeroDaConta;
	
	private String digitoDaConta;
	
	private String nomeDaConta;
	
	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BancoEnum getNumeroDoBanco() {
		return numeroDoBanco;
	}

	public void setNumeroDoBanco(BancoEnum numeroDoBanco) {
		this.numeroDoBanco = numeroDoBanco;
	}

	public String getNumeroDaAgencia() {
		return numeroDaAgencia;
	}

	public void setNumeroDaAgencia(String numeroDaAgencia) {
		this.numeroDaAgencia = numeroDaAgencia;
	}

	public String getDigitoDaAgencia() {
		return digitoDaAgencia;
	}

	public void setDigitoDaAgencia(String digitoDaAgencia) {
		this.digitoDaAgencia = digitoDaAgencia;
	}

	public String getNumeroDaConta() {
		return numeroDaConta;
	}

	public void setNumeroDaConta(String numeroDaConta) {
		this.numeroDaConta = numeroDaConta;
	}

	public String getDigitoDaConta() {
		return digitoDaConta;
	}

	public void setDigitoDaConta(String digitoDaConta) {
		this.digitoDaConta = digitoDaConta;
	}

	public String getNomeDaConta() {
		return nomeDaConta;
	}

	public void setNomeDaConta(String nomeDaConta) {
		this.nomeDaConta = nomeDaConta;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
}
