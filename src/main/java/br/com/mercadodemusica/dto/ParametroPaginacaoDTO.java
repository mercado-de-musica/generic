package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.TipoDeParametroEnum;

public class ParametroPaginacaoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private String urlPesquisa;

	private TipoDeParametroEnum tipoDeParametroEnum;

	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUrlPesquisa() {
		return urlPesquisa;
	}

	public void setUrlPesquisa(String urlPesquisa) {
		this.urlPesquisa = urlPesquisa;
	}

	public TipoDeParametroEnum getTipoDeParametroEnum() {
		return tipoDeParametroEnum;
	}

	public void setTipoDeParametroEnum(TipoDeParametroEnum tipoDeParametroEnum) {
		this.tipoDeParametroEnum = tipoDeParametroEnum;
	}

	public UsuarioDTO getSite() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
}
