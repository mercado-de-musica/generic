package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.mercadodemusica.SerializerDeserializer.UsuarioDtoDeserializer;
import br.com.mercadodemusica.enums.TipoUsuarioEnum;

@JsonDeserialize(using = UsuarioDtoDeserializer.class)
public abstract class UsuarioDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private String email;
	
	private String senha;

	private TipoUsuarioEnum tipoUsuario;
	
//	private TipoDtoUsuarioEnum tipoDtoUsuario;
	
	@JsonManagedReference(value="telefones")
	private Set<TelefoneDTO> telefones;
	
	private List<TelefoneDTO> telefonesList;
	
	@JsonManagedReference(value="enderecos")
	private Set<EnderecoDTO> enderecos;
	
	private List<EnderecoDTO> enderecosList;

//	@JsonManagedReference(value="interessesDeCompra")
//	private Set<InteressesDeCompraDTO> interessesDeCompra;
	
//	@JsonManagedReference(value="listaDeLikes")
//	private Set<LikeProdutoDTO> listaDeLikes;
	
//	@JsonManagedReference(value="reputacaoVendedor")
//	private List<ReputacaoVendedorDTO> reputacaoVendedor;
	
	private Boolean ativo;
	
	private String ipUsuario;
	
	private Calendar dataDeCadastramento;
	
	private Calendar dataDeUpdate = Calendar.getInstance();

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoUsuarioEnum getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

//	public Set<TelefoneDTO> getTelefones() {
//		return telefones;
//	}
//
//	public void setTelefones(Set<TelefoneDTO> telefones) {
//		this.telefones = telefones;
//	}

//	public TipoDtoUsuarioEnum getTipoDtoUsuario() {
//		return tipoDtoUsuario;
//	}
//
//	public void setTipoDtoUsuario(TipoDtoUsuarioEnum tipoDtoUsuario) {
//		this.tipoDtoUsuario = tipoDtoUsuario;
//	}

	public List<TelefoneDTO> getTelefonesList() {
		return telefonesList;
	}

	public void setTelefonesList(List<TelefoneDTO> telefonesList) {
		this.telefonesList = telefonesList;
	}

//	public Set<EnderecoDTO> getEnderecos() {
//		return enderecos;
//	}
//
//	public void setEnderecos(Set<EnderecoDTO> enderecos) {
//		this.enderecos = enderecos;
//	}

	public List<EnderecoDTO> getEnderecosList() {
		return enderecosList;
	}

	public void setEnderecosList(List<EnderecoDTO> enderecosList) {
		this.enderecosList = enderecosList;
	}

//	public Set<InteressesDeCompraDTO> getInteressesDeCompra() {
//		return interessesDeCompra;
//	}
//
//	public void setInteressesDeCompra(Set<InteressesDeCompraDTO> interessesDeCompra) {
//		this.interessesDeCompra = interessesDeCompra;
//	}
//
//	public Set<LikeProdutoDTO> getListaDeLikes() {
//		return listaDeLikes;
//	}
//
//	public void setListaDeLikes(Set<LikeProdutoDTO> listaDeLikes) {
//		this.listaDeLikes = listaDeLikes;
//	}
//
//	public List<ReputacaoVendedorDTO> getReputacaoVendedor() {
//		return reputacaoVendedor;
//	}
//
//	public void setReputacaoVendedor(List<ReputacaoVendedorDTO> reputacaoVendedor) {
//		this.reputacaoVendedor = reputacaoVendedor;
//	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getIpUsuario() {
		return ipUsuario;
	}

	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	/**
	 * @return the dataDeCadastramento
	 */
	public Calendar getDataDeCadastramento() {
		return dataDeCadastramento;
	}

	/**
	 * @param dataDeCadastramento the dataDeCadastramento to set
	 */
	public void setDataDeCadastramento(Calendar dataDeCadastramento) {
		this.dataDeCadastramento = dataDeCadastramento;
	}

	/**
	 * @return the dataDeUpdate
	 */
	public Calendar getDataDeUpdate() {
		return dataDeUpdate;
	}

	/**
	 * @param dataDeUpdate the dataDeUpdate to set
	 */
	public void setDataDeUpdate(Calendar dataDeUpdate) {
		this.dataDeUpdate = dataDeUpdate;
	}
}
