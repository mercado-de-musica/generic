package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class InteressesDeCompraProdutoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6575376658271013098L;
	
	
	private BigInteger id;
	
	private InteressesDeCompraDTO interesseDeCompra;
	
	private ProdutoDTO infosGeraisProdutoUsuario;
	
	private Boolean enviado = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public InteressesDeCompraDTO getInteresseDeCompra() {
		return interesseDeCompra;
	}

	public void setInteresseDeCompra(InteressesDeCompraDTO interesseDeCompra) {
		this.interesseDeCompra = interesseDeCompra;
	}

	public ProdutoDTO getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(ProdutoDTO infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}
}
