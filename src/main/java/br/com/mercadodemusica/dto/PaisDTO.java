package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.PaisesEnum;

public class PaisDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private PaisesEnum paisEnum;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PaisesEnum getPaisEnum() {
		return paisEnum;
	}

	public void setPaisEnum(PaisesEnum paisEnum) {
		this.paisEnum = paisEnum;
	}
}
