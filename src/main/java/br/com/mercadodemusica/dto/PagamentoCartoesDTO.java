package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.InstituicaoEnum;
import br.com.mercadodemusica.enums.TipoCartaoEnum;

public class PagamentoCartoesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private TipoCartaoEnum tipoCartao;
	
	private InstituicaoEnum instituicao;
	
	private String cofre;
	
	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoCartaoEnum getTipoCartao() {
		return tipoCartao;
	}

	public void setTipoCartao(TipoCartaoEnum tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	public InstituicaoEnum getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(InstituicaoEnum instituicao) {
		this.instituicao = instituicao;
	}

	public String getCofre() {
		return cofre;
	}

	public void setCofre(String cofre) {
		this.cofre = cofre;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
}
