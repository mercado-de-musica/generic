package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.MotivoTagEnum;
import br.com.mercadodemusica.enums.TipoTagEnum;

public class TagDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;

	private String tag;

	private TipoTagEnum tipoTag;

	private MotivoTagEnum motivoTag;

	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public TipoTagEnum getTipoTag() {
		return tipoTag;
	}

	public void setTipoTag(TipoTagEnum tipoTag) {
		this.tipoTag = tipoTag;
	}

	public MotivoTagEnum getMotivoTag() {
		return motivoTag;
	}

	public void setMotivoTag(MotivoTagEnum motivoTag) {
		this.motivoTag = motivoTag;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

}
