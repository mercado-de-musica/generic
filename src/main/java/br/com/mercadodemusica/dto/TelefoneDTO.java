package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.mercadodemusica.enums.TipoDeTelefoneEnum;

public class TelefoneDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private String ddd;
	
	private String telefone;
	
	private TipoDeTelefoneEnum tipoDeTelefone;
	
	@JsonBackReference(value="usuarioTelefone")
	private UsuarioDTO usuario;
	
	private BigInteger idUsuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public TipoDeTelefoneEnum getTipoDeTelefone() {
		return tipoDeTelefone;
	}

	public void setTipoDeTelefone(TipoDeTelefoneEnum tipoDeTelefone) {
		this.tipoDeTelefone = tipoDeTelefone;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public BigInteger getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(BigInteger idUsuario) {
		this.idUsuario = idUsuario;
	}
}
