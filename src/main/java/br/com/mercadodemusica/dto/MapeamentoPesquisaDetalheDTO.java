package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class MapeamentoPesquisaDetalheDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String ip;
	
	private String pesquisa;
	
	private String idProdutoCrypt;
	
	private Calendar data;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(String pesquisa) {
		this.pesquisa = pesquisa;
	}

	public String getIdProdutoCrypt() {
		return idProdutoCrypt;
	}

	public void setIdProdutoCrypt(String idProdutoCrypt) {
		this.idProdutoCrypt = idProdutoCrypt;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}
}
