package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class FotoUsuarioDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String urlFoto;
	
	private Boolean fundoBranco;
	
	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public Boolean getFundoBranco() {
		return fundoBranco;
	}

	public void setFundoBranco(Boolean fundoBranco) {
		this.fundoBranco = fundoBranco;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
	
}
