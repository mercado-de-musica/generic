package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class ContratoCorreiosDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private String codigoAdministrativoCorreios;
	
	private String usuario;
	
	private String senha;
	
	private String idDiretoria;
	
	private String idCartaoPostagem;
	
	private String numeroDoContrato;
	
	@JsonManagedReference
	private Set<NumeroServicosCorreiosDTO> arrayNumerosServicosCorreios;
	
	private Boolean ativo;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCodigoAdministrativoCorreios() {
		return codigoAdministrativoCorreios;
	}

	public void setCodigoAdministrativoCorreios(String codigoAdministrativoCorreios) {
		this.codigoAdministrativoCorreios = codigoAdministrativoCorreios;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getIdDiretoria() {
		return idDiretoria;
	}

	public void setIdDiretoria(String idDiretoria) {
		this.idDiretoria = idDiretoria;
	}

	public String getIdCartaoPostagem() {
		return idCartaoPostagem;
	}

	public void setIdCartaoPostagem(String idCartaoPostagem) {
		this.idCartaoPostagem = idCartaoPostagem;
	}

	public String getNumeroDoContrato() {
		return numeroDoContrato;
	}

	public void setNumeroDoContrato(String numeroDoContrato) {
		this.numeroDoContrato = numeroDoContrato;
	}

	public Set<NumeroServicosCorreiosDTO> getArrayNumerosServicosCorreios() {
		return arrayNumerosServicosCorreios;
	}

	public void setArrayNumerosServicosCorreios(Set<NumeroServicosCorreiosDTO> arrayNumerosServicosCorreios) {
		this.arrayNumerosServicosCorreios = arrayNumerosServicosCorreios;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
