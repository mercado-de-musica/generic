package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class TransferenciaValoresUsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
//	@JsonManagedReference
	private List<ProdutoDTO> listaProduto;
	
	private UsuarioDTO usuario;
	
	private Boolean finalizado = Boolean.FALSE;
	
	private String transferenciaGatewayDePagamento;
	
	private BigDecimal valorTransferido;
	
	private BigDecimal porcentagem = BigDecimal.ZERO;
		
	private ContaBancariaDTO contaUsadaNaTransferencia;
	
	@JsonManagedReference
	private List<StatusTransferenciaDeValorDTO> listaStatusTransferenciaDeValor;
	
	private Boolean desistencia = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public List<ProdutoDTO> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(List<ProdutoDTO> listaProduto) {
		this.listaProduto = listaProduto;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getTransferenciaGatewayDePagamento() {
		return transferenciaGatewayDePagamento;
	}

	public void setTransferenciaGatewayDePagamento(String transferenciaGatewayDePagamento) {
		this.transferenciaGatewayDePagamento = transferenciaGatewayDePagamento;
	}

	public BigDecimal getValorTransferido() {
		return valorTransferido;
	}

	public void setValorTransferido(BigDecimal valorTransferido) {
		this.valorTransferido = valorTransferido;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public ContaBancariaDTO getContaUsadaNaTransferencia() {
		return contaUsadaNaTransferencia;
	}

	public void setContaUsadaNaTransferencia(ContaBancariaDTO contaUsadaNaTransferencia) {
		this.contaUsadaNaTransferencia = contaUsadaNaTransferencia;
	}

	public List<StatusTransferenciaDeValorDTO> getListaStatusTransferenciaDeValor() {
		return listaStatusTransferenciaDeValor;
	}

	public void setListaStatusTransferenciaDeValor(List<StatusTransferenciaDeValorDTO> listaStatusTransferenciaDeValor) {
		this.listaStatusTransferenciaDeValor = listaStatusTransferenciaDeValor;
	}

	public Boolean getDesistencia() {
		return desistencia;
	}

	public void setDesistencia(Boolean desistencia) {
		this.desistencia = desistencia;
	}
}