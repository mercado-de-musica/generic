package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class CompraBoletoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6084583509109316713L;

	private BigInteger id;
	
	private String idPagamento;
	
	private Calendar vencimentoBoleto;
	
	private String urlBoleto;
	
	private CompraDTO compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(String idPagamento) {
		this.idPagamento = idPagamento;
	}

	public Calendar getVencimentoBoleto() {
		return vencimentoBoleto;
	}

	public void setVencimentoBoleto(Calendar vencimentoBoleto) {
		this.vencimentoBoleto = vencimentoBoleto;
	}

	public String getUrlBoleto() {
		return urlBoleto;
	}

	public void setUrlBoleto(String urlBoleto) {
		this.urlBoleto = urlBoleto;
	}

	public CompraDTO getCompra() {
		return compra;
	}

	public void setCompra(CompraDTO compra) {
		this.compra = compra;
	}
}
