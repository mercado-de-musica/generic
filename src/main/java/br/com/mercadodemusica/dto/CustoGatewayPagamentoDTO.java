package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.TipoCartaoEnum;

public class CustoGatewayPagamentoDTO implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2479622863832139838L;

	private BigInteger id;
	
	private TipoCartaoEnum tipoDePagamento;
	
	private BigDecimal porcentagem;
	
	private BigDecimal custoTransacao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoCartaoEnum getTipoDePagamento() {
		return tipoDePagamento;
	}

	public void setTipoDePagamento(TipoCartaoEnum tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public BigDecimal getCustoTransacao() {
		return custoTransacao;
	}

	public void setCustoTransacao(BigDecimal custoTransacao) {
		this.custoTransacao = custoTransacao;
	}
}
