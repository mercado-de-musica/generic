package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class HistoricoDatasDeCompraDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8732957214618062306L;
	
	private BigInteger id;
	
	private Calendar dataDaCompra;
	
	private Boolean dataAtualizada = Boolean.FALSE;
	
	@JsonBackReference
	private CompraDTO compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Calendar getDataDaCompra() {
		return dataDaCompra;
	}

	public void setDataDaCompra(Calendar dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}

	public Boolean getDataAtualizada() {
		return dataAtualizada;
	}

	public void setDataAtualizada(Boolean dataAtualizada) {
		this.dataAtualizada = dataAtualizada;
	}

	public CompraDTO getCompra() {
		return compra;
	}

	public void setCompra(CompraDTO compra) {
		this.compra = compra;
	}
}
