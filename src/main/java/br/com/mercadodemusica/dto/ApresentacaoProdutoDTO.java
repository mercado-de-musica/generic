package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.TipoApresentacaoEnum;

public class ApresentacaoProdutoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String endereco;
	
	private Boolean thumb;
	
	private TipoApresentacaoEnum tipoApresentacao;
	
    private ProdutoDTO produto;
	
	private BigDecimal scale;
	
    private BigInteger width;
    
    private BigInteger height;
    
    private BigInteger angle;
    
    private BigInteger x;
    
    private BigInteger y;
	
    private String tipoArquivo;
	
	private Boolean ajuste = false;
	
	private Boolean capa = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Boolean getThumb() {
		return thumb;
	}

	public void setThumb(Boolean thumb) {
		this.thumb = thumb;
	}

	public TipoApresentacaoEnum getTipoApresentacao() {
		return tipoApresentacao;
	}

	public void setTipoApresentacao(TipoApresentacaoEnum tipoApresentacao) {
		this.tipoApresentacao = tipoApresentacao;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}

	public BigDecimal getScale() {
		return scale;
	}

	public void setScale(BigDecimal scale) {
		this.scale = scale;
	}

	public BigInteger getWidth() {
		return width;
	}

	public void setWidth(BigInteger width) {
		this.width = width;
	}

	public BigInteger getHeight() {
		return height;
	}

	public void setHeight(BigInteger height) {
		this.height = height;
	}

	public BigInteger getAngle() {
		return angle;
	}

	public void setAngle(BigInteger angle) {
		this.angle = angle;
	}

	public BigInteger getX() {
		return x;
	}

	public void setX(BigInteger x) {
		this.x = x;
	}

	public BigInteger getY() {
		return y;
	}

	public void setY(BigInteger y) {
		this.y = y;
	}

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public Boolean getAjuste() {
		return ajuste;
	}

	public void setAjuste(Boolean ajuste) {
		this.ajuste = ajuste;
	}

	public Boolean getCapa() {
		return capa;
	}

	public void setCapa(Boolean capa) {
		this.capa = capa;
	}
}
