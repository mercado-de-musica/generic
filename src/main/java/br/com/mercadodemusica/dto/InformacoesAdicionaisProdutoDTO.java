package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.mercadodemusica.enums.PaisesDeFabricacaoEnum;
import br.com.mercadodemusica.enums.TipoEnvioEnum;

public class InformacoesAdicionaisProdutoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String numeroDeSerie;
	
	private Integer altura;
	
	private Integer largura;
	
	private Integer comprimento;
	
	private BigDecimal peso;
	
	private BigInteger anoDeFabricacao;

	private PaisesDeFabricacaoEnum paisDeFabricacao;
	
	private TipoEnvioEnum tipoEnvio;
	
	@JsonManagedReference
	private List<ValorFreteCorreioDTO> calculoFrete;
	
	private Boolean termosDePesoAltura;
	
	private ProdutoDTO produto;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}

	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}

	public Integer getAltura() {
		return altura;
	}

	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	public Integer getLargura() {
		return largura;
	}

	public void setLargura(Integer largura) {
		this.largura = largura;
	}

	public Integer getComprimento() {
		return comprimento;
	}

	public void setComprimento(Integer comprimento) {
		this.comprimento = comprimento;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigInteger getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(BigInteger anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public PaisesDeFabricacaoEnum getPaisDeFabricacao() {
		return paisDeFabricacao;
	}

	public void setPaisDeFabricacao(PaisesDeFabricacaoEnum paisDeFabricacao) {
		this.paisDeFabricacao = paisDeFabricacao;
	}

	public TipoEnvioEnum getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(TipoEnvioEnum tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public List<ValorFreteCorreioDTO> getCalculoFrete() {
		return calculoFrete;
	}

	public void setCalculoFrete(List<ValorFreteCorreioDTO> calculoFrete) {
		this.calculoFrete = calculoFrete;
	}

	public Boolean getTermosDePesoAltura() {
		return termosDePesoAltura;
	}

	public void setTermosDePesoAltura(Boolean termosDePesoAltura) {
		this.termosDePesoAltura = termosDePesoAltura;
	}

	public Boolean getTermosDeEnvio() {
		return termosDeEnvio;
	}

	public void setTermosDeEnvio(Boolean termosDeEnvio) {
		this.termosDeEnvio = termosDeEnvio;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}

	private Boolean termosDeEnvio;

}
