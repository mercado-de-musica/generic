package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.util.Calendar;

public class RastreamentoProdutoDiretoDTO extends RastreamentoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigoComprador;
	
	private String codigoVendedor;

	private Calendar dataUsoDoCodigo;
	
	private String codigoInseridoComprador;
	
	private String codigoInseridoVendedor;
	
	private Calendar dataInsercaoCodigoComprador;
	
	private Calendar dataInsercaoCodigoVendedor;
	
	public String getCodigoComprador() {
		return codigoComprador;
	}

	public void setCodigoComprador(String codigoComprador) {
		this.codigoComprador = codigoComprador;
	}

	public String getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public Calendar getDataUsoDoCodigo() {
		return dataUsoDoCodigo;
	}

	public void setDataUsoDoCodigo(Calendar dataUsoDoCodigo) {
		this.dataUsoDoCodigo = dataUsoDoCodigo;
	}

	public String getCodigoInseridoComprador() {
		return codigoInseridoComprador;
	}

	public void setCodigoInseridoComprador(String codigoInseridoComprador) {
		this.codigoInseridoComprador = codigoInseridoComprador;
	}

	public String getCodigoInseridoVendedor() {
		return codigoInseridoVendedor;
	}

	public void setCodigoInseridoVendedor(String codigoInseridoVendedor) {
		this.codigoInseridoVendedor = codigoInseridoVendedor;
	}

	public Calendar getDataInsercaoCodigoComprador() {
		return dataInsercaoCodigoComprador;
	}

	public void setDataInsercaoCodigoComprador(Calendar dataInsercaoCodigoComprador) {
		this.dataInsercaoCodigoComprador = dataInsercaoCodigoComprador;
	}

	public Calendar getDataInsercaoCodigoVendedor() {
		return dataInsercaoCodigoVendedor;
	}

	public void setDataInsercaoCodigoVendedor(Calendar dataInsercaoCodigoVendedor) {
		this.dataInsercaoCodigoVendedor = dataInsercaoCodigoVendedor;
	}
}
