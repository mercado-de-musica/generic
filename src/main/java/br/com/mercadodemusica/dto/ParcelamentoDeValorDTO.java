package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class ParcelamentoDeValorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8392903925004631542L;

	private BigInteger id;
	
	private BigInteger vezesParceladas;
	
	private BigDecimal taxa;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getVezesParceladas() {
		return vezesParceladas;
	}

	public void setVezesParceladas(BigInteger vezesParceladas) {
		this.vezesParceladas = vezesParceladas;
	}

	public BigDecimal getTaxa() {
		return taxa;
	}

	public void setTaxa(BigDecimal taxa) {
		this.taxa = taxa;
	}
}
