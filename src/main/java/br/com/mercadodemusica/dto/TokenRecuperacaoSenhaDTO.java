package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class TokenRecuperacaoSenhaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -830937707531326737L;
	
	
	private BigInteger id;
	
	private UsuarioDTO usuario;
	
	private String token; 
	
	private Boolean ativo;
	
	private Calendar dataRecuperacaoSenha;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Calendar getDataRecuperacaoSenha() {
		return dataRecuperacaoSenha;
	}

	public void setDataRecuperacaoSenha(Calendar dataRecuperacaoSenha) {
		this.dataRecuperacaoSenha = dataRecuperacaoSenha;
	}
}
