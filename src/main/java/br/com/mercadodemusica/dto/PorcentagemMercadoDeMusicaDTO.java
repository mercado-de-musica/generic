package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

public class PorcentagemMercadoDeMusicaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5929255735519299723L;
	
	private BigInteger id;
	
	private BigDecimal porcentagem;
	
	private String porcentagemString;
	
	private Calendar dataDeInsercao;
	
	private Calendar dataDeMudancaNoSistema;
	
	private UsuarioDTO usuario;
	
	private Boolean ativo;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public String getPorcentagemString() {
		return porcentagemString;
	}

	public void setPorcentagemString(String porcentagemString) {
		this.porcentagemString = porcentagemString;
	}

	public Calendar getDataDeInsercao() {
		return dataDeInsercao;
	}

	public void setDataDeInsercao(Calendar dataDeInsercao) {
		this.dataDeInsercao = dataDeInsercao;
	}

	public Calendar getDataDeMudancaNoSistema() {
		return dataDeMudancaNoSistema;
	}

	public void setDataDeMudancaNoSistema(Calendar dataDeMudancaNoSistema) {
		this.dataDeMudancaNoSistema = dataDeMudancaNoSistema;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
