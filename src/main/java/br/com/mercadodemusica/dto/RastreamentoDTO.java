package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public abstract class RastreamentoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4141760808945414614L;
	
	private BigInteger id;
	
	private ProdutoDTO infosGeraisProdutoUsuario;
	
	private UsuarioDTO comprador;
	
	private Boolean finalizado = Boolean.FALSE;
	
	private Calendar dataProcessamento;
	
	private Boolean desistencia = Boolean.FALSE;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public ProdutoDTO getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(ProdutoDTO infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public UsuarioDTO getComprador() {
		return comprador;
	}

	public void setComprador(UsuarioDTO comprador) {
		this.comprador = comprador;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Calendar getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Calendar dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Boolean getDesistencia() {
		return desistencia;
	}

	public void setDesistencia(Boolean desistencia) {
		this.desistencia = desistencia;
	}
	
	
}
