package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.util.Calendar;

import br.com.mercadodemusica.enums.SexoEnum;

public class UsuarioPessoaFisicaDTO extends UsuarioDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	
	private String sobrenome;
	
	private Calendar dataDeNascimento;
	
	private String dataDeNascimentoString;
	
	private String rg;
	
	private String cpf;
	
	private SexoEnum sexo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	

	public Calendar getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Calendar dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public String getDataDeNascimentoString() {
		return dataDeNascimentoString;
	}

	public void setDataDeNascimentoString(String dataDeNascimentoString) {
		this.dataDeNascimentoString = dataDeNascimentoString;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}
}
