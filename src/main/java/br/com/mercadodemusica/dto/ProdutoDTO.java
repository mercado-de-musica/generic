package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

public class ProdutoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private String idCrypt;
	
	private String nome;
	
	private String descricao;
	
	private String urlAcesso;
	
	private Boolean ativo;
	
	private Boolean produtoDoCrawler;
	
	private Boolean produtoNovo;
	
	private Calendar dataInsercaoProduto;
	
	private String precoString;
	
	private BigDecimal preco;
	
	private UsuarioDTO usuario;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIdCrypt() {
		return idCrypt;
	}

	public void setIdCrypt(String idCrypt) {
		this.idCrypt = idCrypt;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUrlAcesso() {
		return urlAcesso;
	}

	public void setUrlAcesso(String urlAcesso) {
		this.urlAcesso = urlAcesso;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getProdutoDoCrawler() {
		return produtoDoCrawler;
	}

	public void setProdutoDoCrawler(Boolean produtoDoCrawler) {
		this.produtoDoCrawler = produtoDoCrawler;
	}

	public Boolean getProdutoNovo() {
		return produtoNovo;
	}

	public void setProdutoNovo(Boolean produtoNovo) {
		this.produtoNovo = produtoNovo;
	}

	public Calendar getDataInsercaoProduto() {
		return dataInsercaoProduto;
	}

	public void setDataInsercaoProduto(Calendar dataInsercaoProduto) {
		this.dataInsercaoProduto = dataInsercaoProduto;
	}

	public String getPrecoString() {
		return precoString;
	}

	public void setPrecoString(String precoString) {
		this.precoString = precoString;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
}
