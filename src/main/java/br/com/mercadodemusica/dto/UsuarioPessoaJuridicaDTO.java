package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.util.Calendar;

public class UsuarioPessoaJuridicaDTO extends UsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String razaoSocial;
	
	private String nomeFantasia;
	
	private String cnpj;
		
	private String nomeTitular;
	
	private String sobrenomeTitular;
	
	private Calendar dataDeNascimentoTitular;
	
	private String dataDeNascimentoTitularString;
	
	private String rgTitular;
	
	private String cpfTitular;
	
//	@Column(nullable = false, name="orgao_expedidor")
//	private String orgaoExpedidor;
//	
//	@Column(nullable = false, name="data_expedicao")
//	private Calendar dataExpedicao;
	
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getSobrenomeTitular() {
		return sobrenomeTitular;
	}

	public void setSobrenomeTitular(String sobrenomeTitular) {
		this.sobrenomeTitular = sobrenomeTitular;
	}

	public Calendar getDataDeNascimentoTitular() {
		return dataDeNascimentoTitular;
	}

	public void setDataDeNascimentoTitular(Calendar dataDeNascimentoTitular) {
		this.dataDeNascimentoTitular = dataDeNascimentoTitular;
	}

	public String getDataDeNascimentoTitularString() {
		return dataDeNascimentoTitularString;
	}

	public void setDataDeNascimentoTitularString(String dataDeNascimentoTitularString) {
		this.dataDeNascimentoTitularString = dataDeNascimentoTitularString;
	}

	public String getRgTitular() {
		return rgTitular;
	}

	public void setRgTitular(String rgTitular) {
		this.rgTitular = rgTitular;
	}

	public String getCpfTitular() {
		return cpfTitular;
	}

	public void setCpfTitular(String cpfTitular) {
		this.cpfTitular = cpfTitular;
	}

//	public String getOrgaoExpedidor() {
//		return orgaoExpedidor;
//	}
//
//	public void setOrgaoExpedidor(String orgaoExpedidor) {
//		this.orgaoExpedidor = orgaoExpedidor;
//	}
//
//	public Calendar getDataExpedicao() {
//		return dataExpedicao;
//	}
//
//	public void setDataExpedicao(Calendar dataExpedicao) {
//		this.dataExpedicao = dataExpedicao;
//	}
}
