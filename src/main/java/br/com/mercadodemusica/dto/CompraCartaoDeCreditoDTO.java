package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.InstituicaoEnum;

public class CompraCartaoDeCreditoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1399985638236233164L;
	
	private BigInteger id;
	
	private InstituicaoEnum instituicao;
	
	private String primeirosNumeros;
	
	private String ultimosNumeros;
	
	private String idPagamento;
	
	private CompraDTO compra;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public InstituicaoEnum getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(InstituicaoEnum instituicao) {
		this.instituicao = instituicao;
	}
	
	public String getPrimeirosNumeros() {
		return primeirosNumeros;
	}

	public void setPrimeirosNumeros(String primeirosNumeros) {
		this.primeirosNumeros = primeirosNumeros;
	}

	public String getUltimosNumeros() {
		return ultimosNumeros;
	}

	public void setUltimosNumeros(String ultimosNumeros) {
		this.ultimosNumeros = ultimosNumeros;
	}

	public String getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(String idPagamento) {
		this.idPagamento = idPagamento;
	}

	public CompraDTO getCompra() {
		return compra;
	}

	public void setCompra(CompraDTO compra) {
		this.compra = compra;
	}
}
