package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class RastreamentoProdutoCorreiosDTO extends RastreamentoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigoPlpCorreio;
	
	private String etiquetaCorreio;
	
	private Integer digitoEtiquetaCorreio;
	
	private BigDecimal valorPostagemProdutoCorreio;
		
	@JsonManagedReference
	private List<StatusPostagemProdutoCorreioDTO> statusPostagemProdutoCorreio;

	public String getCodigoPlpCorreio() {
		return codigoPlpCorreio;
	}

	public void setCodigoPlpCorreio(String codigoPlpCorreio) {
		this.codigoPlpCorreio = codigoPlpCorreio;
	}

	public String getEtiquetaCorreio() {
		return etiquetaCorreio;
	}

	public void setEtiquetaCorreio(String etiquetaCorreio) {
		this.etiquetaCorreio = etiquetaCorreio;
	}

	public Integer getDigitoEtiquetaCorreio() {
		return digitoEtiquetaCorreio;
	}

	public void setDigitoEtiquetaCorreio(Integer digitoEtiquetaCorreio) {
		this.digitoEtiquetaCorreio = digitoEtiquetaCorreio;
	}

	public BigDecimal getValorPostagemProdutoCorreio() {
		return valorPostagemProdutoCorreio;
	}

	public void setValorPostagemProdutoCorreio(BigDecimal valorPostagemProdutoCorreio) {
		this.valorPostagemProdutoCorreio = valorPostagemProdutoCorreio;
	}

	public List<StatusPostagemProdutoCorreioDTO> getStatusPostagemProdutoCorreio() {
		return statusPostagemProdutoCorreio;
	}

	public void setStatusPostagemProdutoCorreio(List<StatusPostagemProdutoCorreioDTO> statusPostagemProdutoCorreio) {
		this.statusPostagemProdutoCorreio = statusPostagemProdutoCorreio;
	}	
}
