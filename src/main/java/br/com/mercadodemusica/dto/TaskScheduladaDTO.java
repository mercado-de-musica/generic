package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.DiaDaSemana;

public class TaskScheduladaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private Integer horaDeDisparo;
	
	private Integer minutoDeDisparo;

	private DiaDaSemana diaDaSemana;

	private Boolean executando;

	private Boolean ativo;
	
	private String randomNome;
	
	private UsuarioDTO usuario;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Integer getHoraDeDisparo() {
		return horaDeDisparo;
	}

	public void setHoraDeDisparo(Integer horaDeDisparo) {
		this.horaDeDisparo = horaDeDisparo;
	}

	public Integer getMinutoDeDisparo() {
		return minutoDeDisparo;
	}

	public void setMinutoDeDisparo(Integer minutoDeDisparo) {
		this.minutoDeDisparo = minutoDeDisparo;
	}

	public DiaDaSemana getDiaDaSemana() {
		return diaDaSemana;
	}

	public void setDiaDaSemana(DiaDaSemana diaDaSemana) {
		this.diaDaSemana = diaDaSemana;
	}

	public Boolean getExecutando() {
		return executando;
	}

	public void setExecutando(Boolean executando) {
		this.executando = executando;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getRandomNome() {
		return randomNome;
	}

	public void setRandomNome(String randomNome) {
		this.randomNome = randomNome;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
}
