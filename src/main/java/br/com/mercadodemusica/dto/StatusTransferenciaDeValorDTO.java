package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.mercadodemusica.enums.StatusTransferenciaDeValoresGatewayDePagamentoEnum;

public class StatusTransferenciaDeValorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3360217759847537196L;
	
	private BigInteger id;
	
	private StatusTransferenciaDeValoresGatewayDePagamentoEnum statusTransferenciaDeValoresGatewayDePagamento;
	
	private Calendar dataTransferencia;
	
	@JsonBackReference
	private TransferenciaValoresUsuarioDTO transferenciaValoresUsuario;
	
	private Boolean ativo = Boolean.FALSE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public StatusTransferenciaDeValoresGatewayDePagamentoEnum getStatusTransferenciaDeValoresGatewayDePagamento() {
		return statusTransferenciaDeValoresGatewayDePagamento;
	}

	public void setStatusTransferenciaDeValoresGatewayDePagamento(
			StatusTransferenciaDeValoresGatewayDePagamentoEnum statusTransferenciaDeValoresGatewayDePagamento) {
		this.statusTransferenciaDeValoresGatewayDePagamento = statusTransferenciaDeValoresGatewayDePagamento;
	}

	public Calendar getDataTransferencia() {
		return dataTransferencia;
	}

	public void setDataTransferencia(Calendar dataTransferencia) {
		this.dataTransferencia = dataTransferencia;
	}

	public TransferenciaValoresUsuarioDTO getTransferenciaValoresUsuario() {
		return transferenciaValoresUsuario;
	}

	public void setTransferenciaValoresUsuario(TransferenciaValoresUsuarioDTO transferenciaValoresUsuario) {
		this.transferenciaValoresUsuario = transferenciaValoresUsuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
