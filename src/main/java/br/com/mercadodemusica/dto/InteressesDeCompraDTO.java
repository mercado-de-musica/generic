package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.DistanciaInteresseDeCompraEnum;

public class InteressesDeCompraDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private BigInteger instrumentoAcessorio;
	
	private BigInteger marca;

	private BigInteger produto;
	
	private BigInteger modelo;
	
//	@JsonBackReference(value="usuario")
	private UsuarioDTO usuario;
	
	private DistanciaInteresseDeCompraEnum distancia;
    
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getInstrumentoAcessorio() {
		return instrumentoAcessorio;
	}

	public void setInstrumentoAcessorio(BigInteger instrumentoAcessorio) {
		this.instrumentoAcessorio = instrumentoAcessorio;
	}

	public BigInteger getMarca() {
		return marca;
	}

	public void setMarca(BigInteger marca) {
		this.marca = marca;
	}

	public BigInteger getProduto() {
		return produto;
	}

	public void setProduto(BigInteger produto) {
		this.produto = produto;
	}

	public BigInteger getModelo() {
		return modelo;
	}

	public void setModelo(BigInteger modelo) {
		this.modelo = modelo;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public DistanciaInteresseDeCompraEnum getDistancia() {
		return distancia;
	}

	public void setDistancia(DistanciaInteresseDeCompraEnum distancia) {
		this.distancia = distancia;
	}
}
