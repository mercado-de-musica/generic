package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("correioslog")
public class XmlCorreiosDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6093509124855437947L;
	
	
	private String tipo_arquivo;
	
	private String versao_arquivo;
	
	private PlpCorreiosXmlDTO plp;
	
	private RemetenteCorreiosXmlDTO remetente;
	
	private String forma_pagamento;
	
	private ObjetoPostalCorreiosXmlDTO objeto_postal;

	public String getTipo_arquivo() {
		return tipo_arquivo;
	}

	public void setTipo_arquivo(String tipo_arquivo) {
		this.tipo_arquivo = tipo_arquivo;
	}

	public String getVersao_arquivo() {
		return versao_arquivo;
	}

	public void setVersao_arquivo(String versao_arquivo) {
		this.versao_arquivo = versao_arquivo;
	}

	public PlpCorreiosXmlDTO getPlp() {
		return plp;
	}

	public void setPlp(PlpCorreiosXmlDTO plp) {
		this.plp = plp;
	}

	public RemetenteCorreiosXmlDTO getRemetente() {
		return remetente;
	}

	public void setRemetente(RemetenteCorreiosXmlDTO remetente) {
		this.remetente = remetente;
	}

	public String getForma_pagamento() {
		return forma_pagamento;
	}

	public void setForma_pagamento(String forma_pagamento) {
		this.forma_pagamento = forma_pagamento;
	}

	public ObjetoPostalCorreiosXmlDTO getObjeto_postal() {
		return objeto_postal;
	}

	public void setObjeto_postal(ObjetoPostalCorreiosXmlDTO objeto_postal) {
		this.objeto_postal = objeto_postal;
	}	
}
