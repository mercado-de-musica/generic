package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("dimensao_objeto")
public class DimensaoObjetoCorreiosXmlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7449568633742093249L;
	
	
	private String tipo_objeto;
	
	private String dimensao_altura;
	
	private String dimensao_largura;
	
	private String dimensao_comprimento;
	
	private String dimensao_diametro;

	public String getTipo_objeto() {
		return tipo_objeto;
	}

	public void setTipo_objeto(String tipo_objeto) {
		this.tipo_objeto = tipo_objeto;
	}

	public String getDimensao_altura() {
		return dimensao_altura;
	}

	public void setDimensao_altura(String dimensao_altura) {
		this.dimensao_altura = dimensao_altura;
	}

	public String getDimensao_largura() {
		return dimensao_largura;
	}

	public void setDimensao_largura(String dimensao_largura) {
		this.dimensao_largura = dimensao_largura;
	}

	public String getDimensao_comprimento() {
		return dimensao_comprimento;
	}

	public void setDimensao_comprimento(String dimensao_comprimento) {
		this.dimensao_comprimento = dimensao_comprimento;
	}

	public String getDimensao_diametro() {
		return dimensao_diametro;
	}

	public void setDimensao_diametro(String dimensao_diametro) {
		this.dimensao_diametro = dimensao_diametro;
	} 
}
