package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("plp")
public class PlpCorreiosXmlDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8943752296876005632L;

	private String id_plp;
	
	private String valor_global;

	private String mcu_unidade_postagem;
	
	private String nome_unidade_postagem;
	
	private String cartao_postagem;

	public String getId_plp() {
		return id_plp;
	}

	public void setId_plp(String id_plp) {
		this.id_plp = id_plp;
	}

	public String getValor_global() {
		return valor_global;
	}

	public void setValor_global(String valor_global) {
		this.valor_global = valor_global;
	}

	public String getMcu_unidade_postagem() {
		return mcu_unidade_postagem;
	}

	public void setMcu_unidade_postagem(String mcu_unidade_postagem) {
		this.mcu_unidade_postagem = mcu_unidade_postagem;
	}

	public String getNome_unidade_postagem() {
		return nome_unidade_postagem;
	}

	public void setNome_unidade_postagem(String nome_unidade_postagem) {
		this.nome_unidade_postagem = nome_unidade_postagem;
	}

	public String getCartao_postagem() {
		return cartao_postagem;
	}

	public void setCartao_postagem(String cartao_postagem) {
		this.cartao_postagem = cartao_postagem;
	}
}
