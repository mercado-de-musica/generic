package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("objeto_postal")
public class ObjetoPostalCorreiosXmlDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5332353841716052340L;
	
	private String numero_etiqueta;
	
	private String codigo_objeto_cliente;
	
	private String codigo_servico_postagem;

	private String cubagem;
	
	private String peso;
	
	private String rt1;
	
	private String rt2;
	
	private DestinatarioCorreiosXmlDTO destinatario;
	
	private NacionalCorreiosXmlDTO nacional;
	
	private ServicoAdicionalCorreiosXmlDTO servico_adicional;
	
	private DimensaoObjetoCorreiosXmlDTO dimensao_objeto;
	
	private String data_postagem_sara;
	
	private String status_processamento;
	
	private String numero_comprovante_postagem;
	
	private String valor_cobrado;
	
	public String getNumero_etiqueta() {
		return numero_etiqueta;
	}

	public void setNumero_etiqueta(String numero_etiqueta) {
		this.numero_etiqueta = numero_etiqueta;
	}

	public String getCodigo_objeto_cliente() {
		return codigo_objeto_cliente;
	}

	public void setCodigo_objeto_cliente(String codigo_objeto_cliente) {
		this.codigo_objeto_cliente = codigo_objeto_cliente;
	}

	public String getCodigo_servico_postagem() {
		return codigo_servico_postagem;
	}

	public void setCodigo_servico_postagem(String codigo_servico_postagem) {
		this.codigo_servico_postagem = codigo_servico_postagem;
	}

	public String getCubagem() {
		return cubagem;
	}

	public void setCubagem(String cubagem) {
		this.cubagem = cubagem;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getRt1() {
		return rt1;
	}

	public void setRt1(String rt1) {
		this.rt1 = rt1;
	}

	public String getRt2() {
		return rt2;
	}

	public void setRt2(String rt2) {
		this.rt2 = rt2;
	}

	public DestinatarioCorreiosXmlDTO getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(DestinatarioCorreiosXmlDTO destinatario) {
		this.destinatario = destinatario;
	}

	public NacionalCorreiosXmlDTO getNacional() {
		return nacional;
	}

	public void setNacional(NacionalCorreiosXmlDTO nacional) {
		this.nacional = nacional;
	}

	public ServicoAdicionalCorreiosXmlDTO getServico_adicional() {
		return servico_adicional;
	}

	public void setServico_adicional(ServicoAdicionalCorreiosXmlDTO servico_adicional) {
		this.servico_adicional = servico_adicional;
	}

	public DimensaoObjetoCorreiosXmlDTO getDimensao_objeto() {
		return dimensao_objeto;
	}

	public void setDimensao_objeto(DimensaoObjetoCorreiosXmlDTO dimensao_objeto) {
		this.dimensao_objeto = dimensao_objeto;
	}

	public String getData_postagem_sara() {
		return data_postagem_sara;
	}

	public void setData_postagem_sara(String data_postagem_sara) {
		this.data_postagem_sara = data_postagem_sara;
	}

	public String getStatus_processamento() {
		return status_processamento;
	}

	public void setStatus_processamento(String status_processamento) {
		this.status_processamento = status_processamento;
	}

	public String getNumero_comprovante_postagem() {
		return numero_comprovante_postagem;
	}

	public void setNumero_comprovante_postagem(String numero_comprovante_postagem) {
		this.numero_comprovante_postagem = numero_comprovante_postagem;
	}

	public String getValor_cobrado() {
		return valor_cobrado;
	}

	public void setValor_cobrado(String valor_cobrado) {
		this.valor_cobrado = valor_cobrado;
	}
}
