package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("destinatario")
public class DestinatarioCorreiosXmlDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6410799701297183290L;
	
	private String nome_destinatario;
	
	private String telefone_destinatario;
	
	private String celular_destinatario;

	private String email_destinatario;
	
	private String logradouro_destinatario;
	
	private String complemento_destinatario;
	
	public String getNome_destinatario() {
		return nome_destinatario;
	}

	public void setNome_destinatario(String nome_destinatario) {
		this.nome_destinatario = nome_destinatario;
	}

	public String getTelefone_destinatario() {
		return telefone_destinatario;
	}

	public void setTelefone_destinatario(String telefone_destinatario) {
		this.telefone_destinatario = telefone_destinatario;
	}

	public String getCelular_destinatario() {
		return celular_destinatario;
	}

	public void setCelular_destinatario(String celular_destinatario) {
		this.celular_destinatario = celular_destinatario;
	}

	public String getEmail_destinatario() {
		return email_destinatario;
	}

	public void setEmail_destinatario(String email_destinatario) {
		this.email_destinatario = email_destinatario;
	}

	public String getLogradouro_destinatario() {
		return logradouro_destinatario;
	}

	public void setLogradouro_destinatario(String logradouro_destinatario) {
		this.logradouro_destinatario = logradouro_destinatario;
	}

	public String getComplemento_destinatario() {
		return complemento_destinatario;
	}

	public void setComplemento_destinatario(String complemento_destinatario) {
		this.complemento_destinatario = complemento_destinatario;
	}

	public String getNumero_end_destinatario() {
		return numero_end_destinatario;
	}

	public void setNumero_end_destinatario(String numero_end_destinatario) {
		this.numero_end_destinatario = numero_end_destinatario;
	}

	private String numero_end_destinatario;
}
