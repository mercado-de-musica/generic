package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("remetente")
public class RemetenteCorreiosXmlDTO implements Serializable{

		private static final long serialVersionUID = -7251548297950143916L;
	
	private String numero_contrato;
	
	private String numero_diretoria;
	
	private String codigo_administrativo;
	
	private String nome_remetente;

	private String logradouro_remetente;
	
	private String numero_remetente;
	
	private String complemento_remetente;
	
	private String bairro_remetente;
	
	private String cep_remetente;
	
	private String cidade_remetente;
	
	private String uf_remetente;
	
	private String telefone_remetente;
	
	private String fax_remetente;
	
	public String getNumero_contrato() {
		return numero_contrato;
	}

	public void setNumero_contrato(String numero_contrato) {
		this.numero_contrato = numero_contrato;
	}

	public String getNumero_diretoria() {
		return numero_diretoria;
	}

	public void setNumero_diretoria(String numero_diretoria) {
		this.numero_diretoria = numero_diretoria;
	}

	public String getCodigo_administrativo() {
		return codigo_administrativo;
	}

	public void setCodigo_administrativo(String codigo_administrativo) {
		this.codigo_administrativo = codigo_administrativo;
	}

	public String getNome_remetente() {
		return nome_remetente;
	}

	public void setNome_remetente(String nome_remetente) {
		this.nome_remetente = nome_remetente;
	}

	public String getLogradouro_remetente() {
		return logradouro_remetente;
	}

	public void setLogradouro_remetente(String logradouro_remetente) {
		this.logradouro_remetente = logradouro_remetente;
	}

	public String getNumero_remetente() {
		return numero_remetente;
	}

	public void setNumero_remetente(String numero_remetente) {
		this.numero_remetente = numero_remetente;
	}

	public String getComplemento_remetente() {
		return complemento_remetente;
	}

	public void setComplemento_remetente(String complemento_remetente) {
		this.complemento_remetente = complemento_remetente;
	}

	public String getBairro_remetente() {
		return bairro_remetente;
	}

	public void setBairro_remetente(String bairro_remetente) {
		this.bairro_remetente = bairro_remetente;
	}

	public String getCep_remetente() {
		return cep_remetente;
	}

	public void setCep_remetente(String cep_remetente) {
		this.cep_remetente = cep_remetente;
	}

	public String getCidade_remetente() {
		return cidade_remetente;
	}

	public void setCidade_remetente(String cidade_remetente) {
		this.cidade_remetente = cidade_remetente;
	}

	public String getUf_remetente() {
		return uf_remetente;
	}

	public void setUf_remetente(String uf_remetente) {
		this.uf_remetente = uf_remetente;
	}

	public String getTelefone_remetente() {
		return telefone_remetente;
	}

	public void setTelefone_remetente(String telefone_remetente) {
		this.telefone_remetente = telefone_remetente;
	}

	public String getFax_remetente() {
		return fax_remetente;
	}

	public void setFax_remetente(String fax_remetente) {
		this.fax_remetente = fax_remetente;
	}

	public String getEmail_remetente() {
		return email_remetente;
	}

	public void setEmail_remetente(String email_remetente) {
		this.email_remetente = email_remetente;
	}

	private String email_remetente;
	
}
