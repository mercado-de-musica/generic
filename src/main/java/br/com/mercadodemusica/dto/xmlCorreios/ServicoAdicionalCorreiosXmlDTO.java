package br.com.mercadodemusica.dto.xmlCorreios;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("servico_adicional")
public class ServicoAdicionalCorreiosXmlDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 711341521382447934L;

	private String valor_declarado;
	
	private String codigo_servico_adicional;

	public String getValor_declarado() {
		return valor_declarado;
	}

	public void setValor_declarado(String valor_declarado) {
		this.valor_declarado = valor_declarado;
	}

	public String getCodigo_servico_adicional() {
		return codigo_servico_adicional;
	}

	public void setCodigo_servico_adicional(String codigo_servico_adicional) {
		this.codigo_servico_adicional = codigo_servico_adicional;
	}	
}
