package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class NumeroServicosCorreiosDTO implements Serializable{

	/**
	 * Tb_Servicos_ECT
	 * para saber se tem sedex 10, normal, etc.
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private Long idServico;
	
	private String numeroServico;
	
	private String nomeServico;
	
	private String descricao;
	
	@JsonBackReference
	private ContratoCorreiosDTO contratoCorreios;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Long getIdServico() {
		return idServico;
	}

	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

	public String getNumeroServico() {
		return numeroServico;
	}

	public void setNumeroServico(String numeroServico) {
		this.numeroServico = numeroServico;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ContratoCorreiosDTO getContratoCorreios() {
		return contratoCorreios;
	}

	public void setContratoCorreios(ContratoCorreiosDTO contratoCorreios) {
		this.contratoCorreios = contratoCorreios;
	}
}
