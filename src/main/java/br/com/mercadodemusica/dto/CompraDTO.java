package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import br.com.mercadodemusica.enums.IntermediadorDePagamentoEnum;
import br.com.mercadodemusica.enums.StatusPagamentoEnum;
import br.com.mercadodemusica.enums.StatusTransacaoEnum;
import br.com.mercadodemusica.enums.TipoPagamentoEnum;

public class CompraDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger id;
	
	private TipoPagamentoEnum tipoPagamento;
	
	private StatusPagamentoEnum statusPagamento;
	
	private StatusTransacaoEnum statusTransacao;
	
	private String idPedido;
	
	private String tokenPedido;
	
	private String mensagem;
	
	private BigDecimal totalPago;
	
	private Integer parcelas;
	
	private Boolean aceitePoliticaDeCompra;

//	@JsonManagedReference
//	private List<RiscosDaTransacaoDTO> riscosDaTransacao;
	
	private Boolean finalizado = Boolean.FALSE;
	
	private Calendar criacao = Calendar.getInstance();
	
	private IntermediadorDePagamentoEnum intermediadorDePagamento;
	
	private BigInteger idComprador;
	
	private String ipComprador;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TipoPagamentoEnum getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public StatusPagamentoEnum getStatusPagamento() {
		return statusPagamento;
	}

	public void setStatusPagamento(StatusPagamentoEnum statusPagamento) {
		this.statusPagamento = statusPagamento;
	}

	public StatusTransacaoEnum getStatusTransacao() {
		return statusTransacao;
	}

	public void setStatusTransacao(StatusTransacaoEnum statusTransacao) {
		this.statusTransacao = statusTransacao;
	}
	
	public String getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}

	public String getTokenPedido() {
		return tokenPedido;
	}

	public void setTokenPedido(String tokenPedido) {
		this.tokenPedido = tokenPedido;
	}

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public BigDecimal getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(BigDecimal totalPago) {
		this.totalPago = totalPago;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public Boolean getAceitePoliticaDeCompra() {
		return aceitePoliticaDeCompra;
	}

	public void setAceitePoliticaDeCompra(Boolean aceitePoliticaDeCompra) {
		this.aceitePoliticaDeCompra = aceitePoliticaDeCompra;
	}

//	public List<RiscosDaTransacaoDTO> getRiscosDaTransacao() {
//		return riscosDaTransacao;
//	}
//
//	public void setRiscosDaTransacao(List<RiscosDaTransacaoDTO> riscosDaTransacao) {
//		this.riscosDaTransacao = riscosDaTransacao;
//	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Calendar getCriacao() {
		return criacao;
	}

	public void setCriacao(Calendar criacao) {
		this.criacao = criacao;
	}

	public IntermediadorDePagamentoEnum getIntermediadorDePagamento() {
		return intermediadorDePagamento;
	}

	public void setIntermediadorDePagamento(IntermediadorDePagamentoEnum intermediadorDePagamento) {
		this.intermediadorDePagamento = intermediadorDePagamento;
	}

	public BigInteger getIdComprador() {
		return idComprador;
	}

	public void setIdComprador(BigInteger idComprador) {
		this.idComprador = idComprador;
	}

	public String getIpComprador() {
		return ipComprador;
	}

	public void setIpComprador(String ipComprador) {
		this.ipComprador = ipComprador;
	}
}
