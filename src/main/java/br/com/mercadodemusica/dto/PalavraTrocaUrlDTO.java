package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.enums.PalavraTrocaUrlEnum;

public class PalavraTrocaUrlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;

	private PalavraTrocaUrlEnum palavraTrocaUrlEnum;

	private ParametroPaginacaoDTO parametroPaginacao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public PalavraTrocaUrlEnum getPalavraTrocaUrlEnum() {
		return palavraTrocaUrlEnum;
	}

	public void setPalavraTrocaUrlEnum(PalavraTrocaUrlEnum palavraTrocaUrlEnum) {
		this.palavraTrocaUrlEnum = palavraTrocaUrlEnum;
	}

	public ParametroPaginacaoDTO getParametroPaginacao() {
		return parametroPaginacao;
	}

	public void setParametroPaginacao(ParametroPaginacaoDTO parametroPaginacao) {
		this.parametroPaginacao = parametroPaginacao;
	}
}
