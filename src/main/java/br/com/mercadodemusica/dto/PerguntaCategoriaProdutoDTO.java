package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class PerguntaCategoriaProdutoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String pergunta;
	
	private Calendar dataPergunta;
	
	private ProdutoDTO infosGeraisProdutoUsuario;
	
	private BigInteger idPai;
	
	private UsuarioDTO usuarioInteressado;
	
	private Boolean enviadoEmail = Boolean.FALSE;
	
	private Calendar dataEnvioEmail;
	
	private Boolean respondido = Boolean.FALSE;
	
	private Boolean encerrarDiscussao = Boolean.FALSE;
	
	private Boolean visualizarPeloComprador = Boolean.TRUE;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public Calendar getDataPergunta() {
		return dataPergunta;
	}

	public void setDataPergunta(Calendar dataPergunta) {
		this.dataPergunta = dataPergunta;
	}

	public ProdutoDTO getProduto() {
		return infosGeraisProdutoUsuario;
	}

	public void setProduto(ProdutoDTO infosGeraisProdutoUsuario) {
		this.infosGeraisProdutoUsuario = infosGeraisProdutoUsuario;
	}

	public BigInteger getIdPai() {
		return idPai;
	}

	public void setIdPai(BigInteger idPai) {
		this.idPai = idPai;
	}

	public UsuarioDTO getUsuarioInteressado() {
		return usuarioInteressado;
	}

	public void setUsuarioInteressado(UsuarioDTO usuarioInteressado) {
		this.usuarioInteressado = usuarioInteressado;
	}

	public Boolean getEnviadoEmail() {
		return enviadoEmail;
	}

	public void setEnviadoEmail(Boolean enviadoEmail) {
		this.enviadoEmail = enviadoEmail;
	}

	public Calendar getDataEnvioEmail() {
		return dataEnvioEmail;
	}

	public void setDataEnvioEmail(Calendar dataEnvioEmail) {
		this.dataEnvioEmail = dataEnvioEmail;
	}

	public Boolean getRespondido() {
		return respondido;
	}

	public void setRespondido(Boolean respondido) {
		this.respondido = respondido;
	}

	public Boolean getEncerrarDiscussao() {
		return encerrarDiscussao;
	}

	public void setEncerrarDiscussao(Boolean encerrarDiscussao) {
		this.encerrarDiscussao = encerrarDiscussao;
	}

	public Boolean getVisualizarPeloComprador() {
		return visualizarPeloComprador;
	}

	public void setVisualizarPeloComprador(Boolean visualizarPeloComprador) {
		this.visualizarPeloComprador = visualizarPeloComprador;
	}
}
