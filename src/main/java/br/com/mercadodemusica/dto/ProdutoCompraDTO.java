package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class ProdutoCompraDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private CompraDTO compra;
	
	private ProdutoDTO produto;
	
	private Calendar dataDaCompra;
	
	private BigInteger idComprador;
	
	private String ipComprador;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public CompraDTO getCompra() {
		return compra;
	}

	public void setCompra(CompraDTO compra) {
		this.compra = compra;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}

	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}

	public Calendar getDataDaCompra() {
		return dataDaCompra;
	}

	public void setDataDaCompra(Calendar dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}

	public BigInteger getIdComprador() {
		return idComprador;
	}

	public void setIdComprador(BigInteger idComprador) {
		this.idComprador = idComprador;
	}

	public String getIpComprador() {
		return ipComprador;
	}

	public void setIpComprador(String ipComprador) {
		this.ipComprador = ipComprador;
	}
}
