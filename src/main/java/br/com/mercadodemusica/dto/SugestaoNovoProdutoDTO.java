package br.com.mercadodemusica.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;

public class SugestaoNovoProdutoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	
	private String sugestao;
	
	private UsuarioDTO usuario;
	
	private BigInteger infosGeraisProduto;
	
	private Boolean acatado;
	
	private Boolean ativo;
	
	private Calendar dataDeEntradaDaSugestao;
	
	private Calendar dataDeVerificacaoDaSugestao;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getSugestao() {
		return sugestao;
	}

	public void setSugestao(String sugestao) {
		this.sugestao = sugestao;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public BigInteger getCategoriaProduto() {
		return infosGeraisProduto;
	}

	public void setCategoriaProduto(BigInteger infosGeraisProduto) {
		this.infosGeraisProduto = infosGeraisProduto;
	}

	public Boolean getAcatado() {
		return acatado;
	}

	public void setAcatado(Boolean acatado) {
		this.acatado = acatado;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Calendar getDataDeEntradaDaSugestao() {
		return dataDeEntradaDaSugestao;
	}

	public void setDataDeEntradaDaSugestao(Calendar dataDeEntradaDaSugestao) {
		this.dataDeEntradaDaSugestao = dataDeEntradaDaSugestao;
	}

	public Calendar getDataDeVerificacaoDaSugestao() {
		return dataDeVerificacaoDaSugestao;
	}

	public void setDataDeVerificacaoDaSugestao(Calendar dataDeVerificacaoDaSugestao) {
		this.dataDeVerificacaoDaSugestao = dataDeVerificacaoDaSugestao;
	}
	
}
