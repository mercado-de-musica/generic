package br.com.mercadodemusica.SerializerDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import br.com.mercadodemusica.dto.EnderecoDTO;
import br.com.mercadodemusica.dto.TelefoneDTO;
import br.com.mercadodemusica.dto.UsuarioCrawlerDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.enums.SexoEnum;
import br.com.mercadodemusica.enums.TipoUsuarioEnum;

public class UsuarioDtoDeserializer extends StdDeserializer<UsuarioDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected UsuarioDtoDeserializer(Class<?> vc) {
		super(vc);
	} 
	
	public UsuarioDtoDeserializer() {
		this(null);
	}
	
	@Override
	public UsuarioDTO deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		JsonNode node = p.getCodec().readTree(p);
		UsuarioDTO usuarioRetornoDto = null;
		if(node.has("cnpj")) {
			usuarioRetornoDto = new UsuarioPessoaJuridicaDTO();
			usuarioRetornoDto = this.verificarCamposPessoaJuridica((UsuarioPessoaJuridicaDTO) usuarioRetornoDto, node);
			
		} else if(node.has("url")) {
			usuarioRetornoDto = new UsuarioCrawlerDTO();
			usuarioRetornoDto = this.verificarCamposCrawler((UsuarioCrawlerDTO) usuarioRetornoDto, node);
			
		} else {
			usuarioRetornoDto = new UsuarioPessoaFisicaDTO();
			usuarioRetornoDto = this.verificarCamposPessoaFisica((UsuarioPessoaFisicaDTO) usuarioRetornoDto, node);
		}
		
		if(node.has("id") && !node.get("id").isNull()) {
			usuarioRetornoDto.setId(node.get("id").bigIntegerValue());
		}
		
		if(node.has("email") && !node.get("email").isNull()) {
			usuarioRetornoDto.setEmail(node.get("email").asText());
		}
		
		if(node.has("senha") && !node.get("senha").isNull()) {
			usuarioRetornoDto.setSenha(node.get("senha").asText());
		}
		
		if(node.has("tipoUsuario") && !node.get("tipoUsuario").isNull()) {
			usuarioRetornoDto.setTipoUsuario(TipoUsuarioEnum.valueOf(node.get("tipoUsuario").asText()));
		}
		
		if(node.has("ativo") && !node.get("ativo").isNull()) {
			usuarioRetornoDto.setAtivo(node.get("ativo").asBoolean());
		}
		
		if(node.has("ipUsuario") && !node.get("ipUsuario").isNull()) {
			usuarioRetornoDto.setIpUsuario(node.get("ipUsuario").asText());
		}
		
		if(node.has("dataDeCadastramento") && !node.get("dataDeCadastramento").isNull()) {
			
			Long dataDeCadastramentoLong = node.get("dataDeCadastramento").asLong();
			Calendar dataCadastramentoCalendar = Calendar.getInstance();
			dataCadastramentoCalendar.setTimeInMillis(dataDeCadastramentoLong);
			usuarioRetornoDto.setDataDeCadastramento(dataCadastramentoCalendar);
		}
		
//		usuarioPessoaJuridicaDto.setTipoDtoUsuario(TipoDtoUsuarioEnum.valueOf(node.get("tipoDtoUsuario").asText()));
		
		List<TelefoneDTO> listaDeTelefoneDtos = new ArrayList<TelefoneDTO>();
		if(node.has("telefonesList")) {
			for(JsonNode telefoneNode : node.get("telefonesList")) {
				ObjectMapper objectMapper = new ObjectMapper();
				TelefoneDTO telefoneDto = objectMapper.treeToValue(telefoneNode, TelefoneDTO.class);
				listaDeTelefoneDtos.add(telefoneDto);
			}
			usuarioRetornoDto.setTelefonesList(listaDeTelefoneDtos);
		}
		
		
		
		List<EnderecoDTO> listaDeEnderecosDtos = new ArrayList<EnderecoDTO>();
		if(node.has("enderecosList")) {
			for(JsonNode enderecoNode : node.get("enderecosList")) {
				ObjectMapper objectMapper = new ObjectMapper();
				EnderecoDTO enderecoDto = objectMapper.treeToValue(enderecoNode, EnderecoDTO.class);
				listaDeEnderecosDtos.add(enderecoDto);
			}
			usuarioRetornoDto.setEnderecosList(listaDeEnderecosDtos);
		}
		
		
		return usuarioRetornoDto;
	}


	
	private UsuarioDTO verificarCamposPessoaJuridica(UsuarioPessoaJuridicaDTO usuarioRetornoDto, JsonNode node) {
		
		if(node.has("cnpj") && !node.get("cnpj").isNull()) {
			usuarioRetornoDto.setCnpj(node.get("cnpj").asText());
		}
		
		if(node.has("razaoSocial") && !node.get("razaoSocial").isNull()) {
			usuarioRetornoDto.setRazaoSocial(node.get("razaoSocial").asText());
		}
		
		if(node.has("nomeFantasia") && !node.get("nomeFantasia").isNull()) {
			usuarioRetornoDto.setNomeFantasia(node.get("nomeFantasia").asText());
		}
		
		if(node.has("nomeTitular") && !node.get("nomeTitular").isNull()) {
			usuarioRetornoDto.setNomeTitular(node.get("nomeTitular").asText());
		}
		
		if(node.has("sobrenomeTitular") && !node.get("sobrenomeTitular").isNull()) {
			usuarioRetornoDto.setSobrenomeTitular(node.get("sobrenomeTitular").asText());
		}
		
		if(node.has("dataDeNascimentoTitularString") && !node.get("dataDeNascimentoTitularString").isNull()) {
			usuarioRetornoDto.setDataDeNascimentoTitularString(node.get("dataDeNascimentoTitularString").asText());
		}
		
		if(node.has("dataDeNascimentoTitular") && !node.get("dataDeNascimentoTitular").isNull()) {
			
			Long dataDeCadastramentoLong = node.get("dataDeNascimentoTitular").asLong();
			Calendar dataCadastramentoCalendar = Calendar.getInstance();
			dataCadastramentoCalendar.setTimeInMillis(dataDeCadastramentoLong);
			usuarioRetornoDto.setDataDeNascimentoTitular(dataCadastramentoCalendar);
		}
		
		if(node.has("rgTitular") && !node.get("rgTitular").isNull()) {
			usuarioRetornoDto.setRgTitular(node.get("rgTitular").asText());
		}
		
		if(node.has("cpfTitular") && !node.get("cpfTitular").isNull()) {
			usuarioRetornoDto.setCpfTitular(node.get("cpfTitular").asText());
		}
		
		return usuarioRetornoDto;
	}

	
	
	private UsuarioDTO verificarCamposPessoaFisica(UsuarioPessoaFisicaDTO usuarioRetornoDto, JsonNode node) {
		if(node.has("nome") && !node.get("nome").isNull()) {
			usuarioRetornoDto.setNome(node.get("nome").asText());
		}
		
		if(node.has("sobrenome") && !node.get("sobrenome").isNull()) {
			usuarioRetornoDto.setSobrenome(node.get("sobrenome").asText());
		}
		
		if(node.has("cpf") && !node.get("cpf").isNull()) {
			usuarioRetornoDto.setCpf(node.get("cpf").asText());
		}
		
		if(node.has("rg") && !node.get("rg").isNull()) {
			usuarioRetornoDto.setRg(node.get("rg").asText());
		}
		
		if(node.has("sexo") && !node.get("sexo").isNull()) {
			usuarioRetornoDto.setSexo(SexoEnum.valueOf(node.get("sexo").asText()));
		}
		
		if(node.has("dataDeNascimentoString") && !node.get("dataDeNascimentoString").isNull()) {
			usuarioRetornoDto.setDataDeNascimentoString(node.get("dataDeNascimentoString").asText());
		}
		
		if(node.has("dataDeNascimento") && !node.get("dataDeNascimento").isNull()) {
			
			Long dataDeNascimento = node.get("dataDeNascimento").asLong();
			Calendar dataNascimentoCalendar = Calendar.getInstance();
			dataNascimentoCalendar.setTimeInMillis(dataDeNascimento);
			usuarioRetornoDto.setDataDeNascimento(dataNascimentoCalendar);
		}
		
		return usuarioRetornoDto;
		
	}
	
	private UsuarioDTO verificarCamposCrawler(UsuarioCrawlerDTO usuarioRetornoDto, JsonNode node) {
		if(node.has("nome") && !node.get("nome").isNull()) {
			usuarioRetornoDto.setNome(node.get("nome").asText());
		}
		
		if(node.has("url") && !node.get("url").isNull()) {
			usuarioRetornoDto.setUrl(node.get("url").asText());
		}
		
		return usuarioRetornoDto;
	}

//	@Override
//	public UsuarioDTO deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//		System.out.println(json);
//		return null;
//	}
//
//	@Override
//	public JsonElement serialize(UsuarioDTO src, Type typeOfSrc, JsonSerializationContext context) {
//		String stringUsuario = null;
//		
//		if(src instanceof UsuarioPessoaJuridicaDTO) {
//			UsuarioPessoaJuridicaDTO srcPessoaJuridica = (UsuarioPessoaJuridicaDTO) src;
//			stringUsuario = SerializerDeserializer.javaObjectToString(srcPessoaJuridica);
//			
//		} else if(src instanceof UsuarioCrawlerDTO) {
//			UsuarioCrawlerDTO srcCrawler = (UsuarioCrawlerDTO) src;
//			stringUsuario = SerializerDeserializer.javaObjectToString(srcCrawler);
//			
//		} else {
//			UsuarioPessoaFisicaDTO srcPessoaFisica = (UsuarioPessoaFisicaDTO) src;
//			stringUsuario = SerializerDeserializer.javaObjectToString(srcPessoaFisica);
//	
//		}
//		
//		return (JsonElement) SerializerDeserializer.stringParaObject(stringUsuario, JsonElement.class);
//	}
}
