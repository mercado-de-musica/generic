package br.com.mercadodemusica.transformacaoDeDados;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

public class Precos {
	
	public String padraoAmericanoParaBrasileiro(BigDecimal preco) {
		
		if(preco.toString().contains(".")) {
			String precoString = StringUtils.replace(preco.toString(), ".", ",");
			
			String[] precoArray = precoString.split(",");
			
			int tamanhoNumero = precoArray[0].length();
			int tamanhoComparacao = 0;
			
			String stringFinal = "";
			while(tamanhoNumero != 0) {
//				if(tamanhoNumero - 4 >= 0) {
//					
//					tamanhoComparacao = tamanhoNumero - 4;
//					
//					String trecho = precoArray[0].substring(tamanhoNumero - 4, tamanhoNumero);
//					stringFinal = trecho + "." + stringFinal;
//					
//				} 
				if(tamanhoNumero - 3 >= 0) {
					
					tamanhoComparacao = tamanhoNumero - 3;
					
					String trecho = precoArray[0].substring(tamanhoNumero - 3, tamanhoNumero);
					stringFinal = trecho + "." + stringFinal;
					
				} else if(tamanhoNumero - 2 >= 0) {
					
					tamanhoComparacao = tamanhoNumero - 2;
					
					String trecho = precoArray[0].substring(tamanhoNumero - 2, tamanhoNumero);
					stringFinal = trecho + "." + stringFinal;
					
				} else if(tamanhoNumero - 1 >= 0) {
					
					tamanhoComparacao = tamanhoNumero - 1;
					
					String trecho = precoArray[0].substring(tamanhoNumero - 1, tamanhoNumero);
					stringFinal = trecho + "." + stringFinal;
					
				}
				
				tamanhoNumero = tamanhoComparacao;
			}
			
			stringFinal = stringFinal.substring(0, stringFinal.length() - 1);
			
			return stringFinal + "," + precoArray[1].substring(0, 2);
		} else {
			return preco.toString();
		}
		
		
	}

	public BigDecimal padraoBrasileiroParaBigDecimal(String valor) {		
		return new BigDecimal(StringUtils.replace(StringUtils.replace(valor, ".", ""), ",", "."));
	}
	
}
