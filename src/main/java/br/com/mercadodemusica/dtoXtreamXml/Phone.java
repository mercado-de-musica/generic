package br.com.mercadodemusica.dtoXtreamXml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("phone")
public class Phone {
	
	private String areacode;
	
	private String number;

	public String getAreacode() {
		return areacode;
	}

	public String getNumber() {
		return number;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
