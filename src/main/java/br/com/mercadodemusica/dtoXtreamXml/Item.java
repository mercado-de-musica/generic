package br.com.mercadodemusica.dtoXtreamXml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class Item {
	
	private String id;
	
	private String description;
	
	private String amount;
    
	private String quantity;
    
	private String weight;

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getAmount() {
		return amount;
	}

	public String getQuantity() {
		return quantity;
	}

	public String getWeight() {
		return weight;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
}
