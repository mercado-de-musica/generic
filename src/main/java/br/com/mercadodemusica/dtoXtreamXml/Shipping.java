package br.com.mercadodemusica.dtoXtreamXml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("shipping")
public class Shipping {

	private String type;
	
	private Address address;

	public String getType() {
		return type;
	}

	public Address getAddress() {
		return address;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
