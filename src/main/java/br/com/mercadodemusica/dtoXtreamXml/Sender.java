package br.com.mercadodemusica.dtoXtreamXml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("sender")
public class Sender {
	
	private String name;
	
	private String email;
	
	private Phone phone;

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}
}
