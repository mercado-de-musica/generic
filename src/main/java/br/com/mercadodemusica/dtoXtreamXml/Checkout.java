package br.com.mercadodemusica.dtoXtreamXml;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("checkout")
public class Checkout {
	
	private String currency;
	
	@XStreamAlias("items")
	private List<Item> items;
	
	private String reference;
	
	private Sender sender;
	
	private Shipping shipping;

	public String getCurrency() {
		return currency;
	}

	public List<Item> getItems() {
		return items;
	}

	public String getReference() {
		return reference;
	}

	public Sender getSender() {
		return sender;
	}

	public Shipping getShipping() {
		return shipping;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}
	
}
